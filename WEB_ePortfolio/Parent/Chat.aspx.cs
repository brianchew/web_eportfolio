﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using WEB_ePortfolio.Classes;
using System.Diagnostics;
using System.Configuration;

namespace WEB_ePortfolio.Parent.Mentor
{
    public partial class Chat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int id= Convert.ToInt16(Session["ParentID"]);
            LoadMessage(id);
        }
        private void LoadMessage(int pid)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT * FROM Message m INNER JOIN Parent p ON m.FromID=p.ParentID WHERE FromID=@parentid", conn);
            cmd.Parameters.AddWithValue("@parentid", pid);
            SqlDataAdapter daMsgs = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daMsgs.Fill(result, "Messages");
            conn.Close();

            gvMessages.DataSource = result.Tables["Messages"];
            gvMessages.DataBind();
        }
        private void displayReplies(int id, int msgid)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT * FROM Reply WHERE MessageID=@msgid OR ParentID=@parentid", conn);
            cmd.Parameters.AddWithValue("@msgid", msgid);
            cmd.Parameters.AddWithValue("@parentid", id);
            SqlDataAdapter daReplies = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daReplies.Fill(result, "Replies");
            conn.Close();

            gvRepliesFroMentor.DataSource = result.Tables["Replies"];
            gvRepliesFroMentor.DataBind();
        }
        protected void gvMessages_SelectedIndexChanged(object sender, EventArgs e)
        {
            int msgID = Convert.ToInt32(gvMessages.SelectedDataKey[0]);
            int parentID = Convert.ToInt32(Session["ParentID"]);
            displayReplies(parentID, msgID);
        }
        private int LoadMentorIDFromStudentID(int studentID)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT MentorID FROM Student WHERE StudentID=@studentid", conn);
            cmd.Parameters.AddWithValue("@studentid", studentID);
            conn.Open();
            int mentorID = Convert.ToInt16(cmd.ExecuteScalar());
            conn.Close();
            return mentorID;
        }
        private int getStudentID(int parentID)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT StudentID FROM ViewingRequest WHERE ParentID=@parentid AND StudentID IS NOT NULL", conn);
            cmd.Parameters.AddWithValue("@parentid", parentID);
            conn.Open();
            int studentID = Convert.ToInt16(cmd.ExecuteScalar());
            conn.Close();
            return studentID;
        }
        protected void btnSendMessage_Click(object sender, EventArgs e)//New Messaeg
        {
            if(!string.IsNullOrEmpty(tbMessage.Text)&& !string.IsNullOrEmpty(tbTitle.Text))
            {
                MessageClass objMsg = new MessageClass();
                int parentID = Convert.ToInt32(Session["ParentID"]);
                int mentorID = LoadMentorIDFromStudentID(getStudentID(parentID));
                objMsg.NewMessage(parentID, mentorID, tbTitle.Text, tbMessage.Text);
            }
            else
            {
                lblStatus.Text = "Please type in a title, and message";
            }
        }

        protected void btnSendMessageSecond_Click(object sender, EventArgs e)//Reply
        {
            if (!string.IsNullOrEmpty(tbReplyToMessage.Text))
            {
                int msgID = Convert.ToInt32(gvMessages.SelectedDataKey[0]);
                ReplyClass objReply = new ReplyClass();
                objReply.MessageID = msgID;
                objReply.ParentID = Convert.ToInt16(Session["ParentID"]);
                objReply.DateTimePosted = DateTime.UtcNow;
                objReply.Text = tbReplyToMessage.Text.ToString();

                if (objReply.ReplyMessageParent())
                {
                    lblStatus.Text = "Reply was sent to the database.";
                }
                else
                {
                    lblStatus.Text = "Please try again later.";

                }
            }
            else
            {
                lblStatus.Text = "Please type in a message";
            }
        }
    }
}