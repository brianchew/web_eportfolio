﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace WEB_ePortfolio.Parent
{   
    public partial class CreateAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            


        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (txtName.Text == "")
            {
                lblName.Text = "Please input name";
            }
            if (txtUser.Text == "")
            {
                lblUser.Text = "Please input email";
            }
            if (txtPass.Text == "")
            {
                lblPass.Text = "Please input password";
            }
            if (txtPass.Text == txtUser.Text)
            {
                lblPassmatch.Text = "Password matches username";
            }
            if (txtConfirmpass.Text != txtPass.Text)
            {
                lblConfirmPass.Text = "Password does not match";
            }

            if (txtName.Text != "" && txtUser.Text != "" && txtPass.Text != "" && txtUser.Text != txtPass.Text && txtConfirmpass.Text == txtPass.Text)
            {
                string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("INSERT INTO Parent (ParentName, EmailAddr, Password) VALUES (@parentname, @emailaddr, @password)", conn);
                cmd.Parameters.AddWithValue("@parentname", txtName.Text);
                cmd.Parameters.AddWithValue("@emailaddr", txtUser.Text);
                cmd.Parameters.AddWithValue("@password", txtPass.Text);
                SqlDataAdapter daStud = new SqlDataAdapter(cmd);
                DataSet result = new DataSet();

                conn.Open();
                daStud.Fill(result, "StudDetails");
                conn.Close();

                Response.Redirect("ConfirmAccount.aspx");
            }

        }
    }
}