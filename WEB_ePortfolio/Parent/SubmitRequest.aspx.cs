﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace WEB_ePortfolio.Parent
{
    public partial class SubmitRequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {



        
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            string name = txtName.Text;
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("Select * From Student Where Name=@name", conn);
            cmd.Parameters.AddWithValue("@name", name.ToLower());
            SqlDataAdapter daStud = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daStud.Fill(result, "StudDetails");
            conn.Close();

            if (result.Tables["StudDetails"].Rows.Count < 1)
            {
                lblMsg.Text = "Student does not exist";
                return;
            }
            string email = Session["LoginID"].ToString();
            cmd = new SqlCommand("Insert Into ViewingRequest (ParentID, StudentName, StudentID, DateCreated) Values ((Select ParentID From Parent Where EmailAddr=@email), @studentname, @studentid, GetDate())", conn);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@studentname", name);
            cmd.Parameters.AddWithValue("@studentid", Convert.ToInt16(result.Tables["StudDetails"].Rows[0]["StudentID"]));

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("Index.aspx");

        }
    }
}