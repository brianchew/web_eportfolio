﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Parent/Parent.Master" AutoEventWireup="true" CodeBehind="CreateAccount.aspx.cs" Inherits="WEB_ePortfolio.Parent.CreateAccount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 144px;
        }
        .auto-style2 {
            width: 144px;
            height: 26px;
        }
        .auto-style3 {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="container-fluid h-100 bg-secondary" style="height: 103px">
                <br />
            <div class="container mb-5 p-5 card text-white shadow-lg bg-secondary form-group" style="left: -139px; top: 193px">
    <table class="w-100">
        <tr>
            <td class="auto-style2">Name:</td>
            <td class="auto-style3">
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                <asp:Label ID="lblName" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style3"></td>
        </tr>
        <tr>
            <td class="auto-style2">Username:</td>
            <td class="auto-style3">
                <asp:TextBox ID="txtUser" runat="server" TextMode="Email"></asp:TextBox>
                <asp:Label ID="lblUser" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style3">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style1">Password:</td>
            <td>
                <asp:TextBox ID="txtPass" runat="server"></asp:TextBox>
                <asp:Label ID="lblPass" runat="server" ForeColor="Red"></asp:Label>
                <asp:Label ID="lblPassmatch" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">Confirm Password:</td>
            <td class="auto-style3">
                <asp:TextBox ID="txtConfirmpass" runat="server"></asp:TextBox>
                <asp:Label ID="lblConfirmPass" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style3">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style3">
                <asp:Button ID="btnConfirm" runat="server" Text="Create Account" CssClass="btn btn-primary" OnClick="btnConfirm_Click" />
            </td>
        </tr>
    </table>
                </div>
            </div>
</asp:Content>
