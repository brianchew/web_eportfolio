﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ParentMenu.ascx.cs" Inherits="WEB_ePortfolio.Parent.ParentMenu" %>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
    <a class="navbar-brand" href="Main.aspx" style="font-size:32px; font-weight:bold; color:white;">
        ABC Polytechnic
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#staffNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="staffNavbar">
        <ul class="navbar-nav mr-auto ">
            <li class="nav-item text-white">
                <a class="nav-link" href="Index.aspx">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="CreateAccount.aspx">Create Account</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="SubmitRequest.aspx">Submit Viewing Request</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="ViewPortfolio.aspx">View Child Portfolio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Chat.aspx">Mentor Chat</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <asp:Button ID="btnLogOut" runat="server" Text="Log Out" OnClick="btnLogOut_Click" />
        </ul>
    </div>
</nav>