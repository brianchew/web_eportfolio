﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_ePortfolio.Parent
{
    public partial class ViewPortfolio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                string email = Session["LoginID"].ToString();
                string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("SELECT * FROM ViewingRequest WHERE ParentID = (SELECT ParentID FROM Parent WHERE EmailAddr = @email)", conn);
                cmd.Parameters.AddWithValue("@email", email);
                SqlDataAdapter daRequest = new SqlDataAdapter(cmd);
                DataSet request = new DataSet();
                conn.Open();
                daRequest.Fill(request, "Parent");
                conn.Close();

                if (request.Tables["Parent"].Rows.Count > 0)
                {
                    for (int i = 0; i < request.Tables["Parent"].Rows.Count; i++)
                    {
                        if (request.Tables["Parent"].Rows[i]["Status"].ToString() == "A")
                        {
                            cmd = new SqlCommand("SELECT * FROM Student Where StudentID = @StudentID", conn);
                            cmd.Parameters.AddWithValue("@StudentID", Convert.ToInt16(request.Tables["Parent"].Rows[i]["StudentID"]));
                            SqlDataAdapter daStud = new SqlDataAdapter(cmd);
                            DataSet result = new DataSet();

                            conn.Open();
                            daStud.Fill(result, "Student");
                            conn.Close();

                            lblName.Text = (result.Tables["Student"].Rows[0]["Name"].ToString());
                            txtAchievement.Text = (result.Tables["Student"].Rows[0]["Achievement"].ToString());
                            txtDescription.Text = (result.Tables["Student"].Rows[0]["Description"].ToString());
                            return;
                        }
                        else if (request.Tables["Parent"].Rows[i]["Status"].ToString() == "P")
                        {
                            Response.Redirect("PendingRequest.aspx");
                        }

                        else
                        {
                            //Display that viewing request was rejected or pending
                            Response.Redirect("RejectRequest.aspx");
                        }
                    }
                }


                else
                {
                    //Display that no viewing request was sent
                    Response.Redirect("NoRequest.aspx");
                }
            }
                        

                /*string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("SELECT Name FROM Student", conn);
                SqlDataAdapter daStud = new SqlDataAdapter(cmd);
                DataSet result = new DataSet();

                conn.Open();
                daStud.Fill(result, "Student");
                conn.Close();

                for (int i = 0; i < result.Tables["Student"].Rows.Count; i++)
                {
                    ddlName.Items.Add(result.Tables["Student"].Rows[i]["Name"].ToString());
                }*/
        }
            
    }
        /*
        protected void btnLoad_Click(object sender, EventArgs e)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT Description,Achievement FROM Student Where Name=@name", conn);
            cmd.Parameters.AddWithValue("@name", ddlName.SelectedValue);
            SqlDataAdapter daStud = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daStud.Fill(result, "Student");
            conn.Close();

            for (int i = 0; i < result.Tables["Student"].Rows.Count; i++)
            {
                txtAchievement.Text = (result.Tables["Student"].Rows[0]["Achievement"].ToString());
                txtDescription.Text = (result.Tables["Student"].Rows[0]["Description"].ToString());
            }*/
 }
    

