﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="WEB_ePortfolio.Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="bg-dark border-top border-primary pb-4">
        <h1 class="py-3 text-center text-white">Need to contact us regarding admission?</h1>
        <div class="container text-white bg-dark w-100 h-75 rounded">
            <div class="form-group pt-2">
                <label for="exampleInputEmail1">Your name</label>
                <input type="email" class="form-control" id="yourname" aria-describedby="emailHelp" placeholder="Enter your name">
                <small id="namehelp" class="form-text text-muted">How we can address you.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Your message</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder=""></textarea>
            </div>
            <button type="button" class="btn btn-primary btn-lg btn-block">Send your message <i class="fas fa-paper-plane"></i></button>
        </div>
    </div>
</asp:Content>
