﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Menu.ascx.cs" Inherits="WEB_ePortfolio.Menu" %>
<nav class="navbar navbar-expand-lg bg-dark navbar-dark">
    <div class="container">
    <a class="navbar-brand" href="Index.aspx" style="font-size:32px; font-weight:bold; color:white;">
        <img src="Assets/logo.png" />
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#staffNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="staffNavbar">
        <ul class="navbar-nav mr-auto ">
            <li class="nav-item text-white">
                <a class="nav-link" href="Index.aspx">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="About.aspx">About Us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="ePortfolio.aspx">ePortfolio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Contact.aspx">Contact</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Quick-Redirect
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="Student/Index.aspx">Student</a>
                    <a class="dropdown-item" href="Parent/Index.aspx">Parents</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="Mentor/Index.aspx">Mentor</a>
                    <a class="dropdown-item" href="/SysAdmin/Index.aspx">Admin</a>
                </div>
            </li>

        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="Login.aspx">Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Register.aspx">Register</a>
            </li>
        </ul>
    </div>
        </div>
</nav>