﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="WEB_ePortfolio.About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .our-team-main {
            width: 100%;
            height: auto;
            border-bottom: 5px #323233 solid;
            background: #fff;
            text-align: center;
            border-radius: 10px;
            overflow: hidden;
            position: relative;
            transition: 0.5s;
            margin-bottom: 28px;
        }


            .our-team-main img {
                border-radius: 50%;
                margin-bottom: 20px;
                width: 90px;
            }

            .our-team-main h3 {
                font-size: 20px;
                font-weight: 700;
            }

            .our-team-main p {
                margin-bottom: 0;
            }

        .team-back {
            width: 100%;
            height: auto;
            position: absolute;
            top: 0;
            left: 0;
            padding: 5px 15px 0 15px;
            text-align: left;
            background: #3c3d41;
        }

        .team-front {
            width: 100%;
            height: auto;
            position: relative;
            z-index: 10;
            background: #3c3d41;
            padding: 15px;
            bottom: 0px;
            transition: all 0.5s ease;
        }

        .our-team-main:hover .team-front {
            bottom: -200px;
            transition: all 0.5s ease;
        }

        .our-team-main:hover {
            border-color: #777;
            transition: 0.5s;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="bg-secondary">
        <div class="container text-white">
            <h1 class=" text-center py-5">Pioneers of Education</h1>
            <div class="container">
                <p class="text-justify">
                    As one of the leading education institutes around the world, we specialize in training students to become renowned IT professionals. Some of our alumni matter : <strong>Donald J. Trump, Alan Turring</strong> and even more. We also allow students to choose their ideal technology pathway whlie they study. From working with the top research teams around the globe, to working at leading startups in the industry, our students get to pick. Furthermore, we house our own education infrastructure, from providing students with the chance to set up their own servers, to developing their very first applications to be published to the public.
                </p>
            </div>
            <div class="container">
                <h1 class=" text-center py-2">Some of our partners</h1>
                <p class="lead text-center">
                    We work with a variety of industry leaders here at ABC Polytechnic.
                </p>
                <div class="row">
                    <div class="col">
                        <img class="img-fluid" src="http://ngeeann.com.sg/images/Poly-logo2.gif" alt="logo" />
                    </div>
                    <div class="col">
                        <img class="img-fluid" src="https://www.6seconds.org/wp-content/uploads/2015/08/SP-Logo.png" alt="logo" />

                    </div>
                    <div class="col">
                        <img class="img-fluid" src="https://www.chiefwineofficer.com/wp-content/uploads/2016/02/microsoft-logo-white-png.png" alt="logo" />

                    </div>
                    <div class="col">
                        <img class="img-fluid" src="http://fintechnews.sg/wp-content/uploads/2017/11/IMDA.png" alt="logo" />

                    </div>
                </div>
                <div class="row py-4">
                    <div class="col">
                        <img class="img-fluid" src="https://rpstaffdirectory.rp.edu.sg/Content/img/RP%20Logo-CMYK-High-Res.png" alt="logo" />
                    </div>
                    <div class="col">
                        <img class="img-fluid" src="https://lh4.googleusercontent.com/eqrHZmpPHTOHl9awYgD2WDajsLg6U6F8tiOvCuBAyuirIx-qOdZKXnEQM7N5sA-aCEbEZbLx1UwAKZvMmHMkMHcpXf_PXBmhTR3OUdLUCCJzC8AekbB9yZcXCvUz11zJPqHqDArQ" alt="logo" />

                    </div>
                    <div class="col">
                        <img class="img-fluid" src="http://www.stickpng.com/assets/images/580b57fcd9996e24bc43c51f.png" alt="logo" />

                    </div>
                    <div class="col">
                        <img class="img-fluid" src="https://upload.wikimedia.org/wikipedia/commons/1/1c/Equinix_logo.png" alt="logo" />

                    </div>
                </div>
            </div>
            <div class="container">
                <h1 class=" text-center py-2">The board</h1>
                <p class="lead text-center">
                    The team of expierenced leaders leading the school.
                </p>
                <div class="row">
                                        <div class="col">
                        <div class="our-team-main">

                            <div class="team-front">
                                <img src="http://joelduggan.com/wp-content/gallery/cartoon-avatars/star-scav-2012-02-gregc-300x-twitter.png" class="img-fluid" />
                                <h3>Junkang Ong</h3>
                                <p>Council Head</p>
                            </div>

                            <div class="team-back">
                                <span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis 
	natoque penatibus et magnis dis parturient montes,
	Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis 
	natoque.
	</span>
                            </div>

                        </div>
                    </div>

                    <div class="col">
                        <div class="our-team-main">

                            <div class="team-front">
                                <img src="https://i.imgur.com/n5Bej6Z.jpg" class="img-fluid" />
                                <h3>Eric Loh</h3>
                                <p>Dean</p>
                            </div>

                            <div class="team-back">
                                <span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis 
	natoque penatibus et magnis dis parturient montes,
	Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis 
	natoque.
	</span>
                            </div>

                        </div>
                    </div>

                    <div class="col">
                        <div class="our-team-main">

                            <div class="team-front">
                                <img src="https://i.imgur.com/e4F8yoQ.jpg" class="img-fluid" />
                                <h3>Stanley Peh</h3>
                                <p>Finance Advisor</p>
                            </div>

                            <div class="team-back">
                                <span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis 
	natoque penatibus et magnis dis parturient montes,
	Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis 
	natoque.
	</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
