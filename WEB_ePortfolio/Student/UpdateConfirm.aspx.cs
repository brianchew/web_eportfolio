﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using WEB_ePortfolio.Classes;

namespace WEB_ePortfolio.Student
{
    public partial class UpdateConfirm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string achievements = Request.QueryString["achievements"];
                string selfdesc = Request.QueryString["selfdesc"];

                txtAchievements.Text = achievements;
                txtSelfDesc.Text = selfdesc;

                string skillsets = Request.QueryString["skillsets"];
                string[] skillsetslist = skillsets.Split(',');
                for(int i = 0; i < skillsetslist.Count(); i++)
                {
                    blSkillSets.Items.Add(skillsetslist[i]);
                }
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            StudentClass stud = new StudentClass();
            stud.Email = Session["LoginID"].ToString();
            stud.getDetails();
            stud.Achievement = Request.QueryString["achievements"];
            stud.Description = Request.QueryString["selfdesc"];
            stud.update();

            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("DELETE FROM StudentSkillSet WHERE StudentID=@studentid", conn);
            cmd.Parameters.AddWithValue("@studentid", stud.StudentID);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();


            

            foreach (ListItem item in blSkillSets.Items)
            {
                cmd = new SqlCommand("SELECT SkillSetID FROM SkillSet WHERE SkillSetName=@skillsetname", conn);
                cmd.Parameters.AddWithValue("@skillsetname", item.Text);
                SqlDataAdapter daStud = new SqlDataAdapter(cmd);
                DataSet result = new DataSet();
                conn.Open();
                daStud.Fill(result ,"StudDetails");
                conn.Close();

                SqlCommand cmd2 = new SqlCommand("INSERT INTO StudentSkillSet (StudentID, SkillSetID) VALUES (@studentid, @skillsetid)", conn);
                cmd2.Parameters.AddWithValue("@studentid", stud.StudentID);
                cmd2.Parameters.AddWithValue("@skillsetid", Convert.ToInt16(result.Tables["StudDetails"].Rows[0]["SkillSetID"]));
                conn.Open();
                cmd2.ExecuteNonQuery();
                conn.Close();
            }

            Response.Redirect("Index.aspx");
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Update.aspx");
        }
    }
}