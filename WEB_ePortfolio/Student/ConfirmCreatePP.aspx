﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/Student.Master" AutoEventWireup="true" CodeBehind="ConfirmCreatePP.aspx.cs" Inherits="WEB_ePortfolio.Student.ConfirmCreatePP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 26px;
        }
        .auto-style2 {
            height: 26px;
            width: 200px;
        }
        .auto-style3 {
            width: 200px;
        }
        .auto-style4 {
            height: 31px;
            width: 200px;
        }
        .auto-style5 {
            height: 31px;
        }
        .auto-style6 {
            width: 200px;
            height: 57px;
        }
        .auto-style7 {
            height: 57px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid h-100 bg-secondary">
        <div class="container shadow-lg mb-5 p-5 card text-white bg-secondary">
            <table class="w-100">
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style1">Confirm Creation?</td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style4">Project Title:</td>
                    <td class="auto-style5">
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">Project Description:</td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtDesc" runat="server" Enabled="False" Height="100px" TextMode="MultiLine" Width="250px" CssClass="form-control"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style6">Members:</td>
                    <td class="auto-style7">
                        <asp:BulletedList ID="blMember" runat="server" Width="250px">
                        </asp:BulletedList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style6">Project Poster:</td>
                    <td class="auto-style7">
                        <asp:Image ID="imgProjPoster" runat="server" Height="500px" Width="500px" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" CssClass="form-control" Width="100px"/>
                        <br />
                        <asp:Button ID="btnConfirm" runat="server" Text="Confirm" OnClick="btnConfirm_Click" CssClass="form-control" Width="100px"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
