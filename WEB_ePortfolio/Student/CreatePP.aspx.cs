﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using WEB_ePortfolio.Classes;

namespace WEB_ePortfolio.Student
{
    public partial class CreatePP : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string email = Session["LoginID"].ToString();
                StudentClass stud = new StudentClass();
                stud.Email = email;
                stud.getDetails();

                string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("SELECT Name FROM Student WHERE EmailAddr!=@emailaddr", conn);
                cmd.Parameters.AddWithValue("@emailaddr", stud.Email);
                SqlDataAdapter daStud = new SqlDataAdapter(cmd);
                DataSet result = new DataSet();

                conn.Open();
                daStud.Fill(result, "StudDetails");
                conn.Close();

                for (int i = 0; i < result.Tables["StudDetails"].Rows.Count; i++)
                {
                    ddlName.Items.Add(result.Tables["StudDetails"].Rows[i]["Name"].ToString());
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            string name = ddlName.SelectedValue;

            int count = 0;
            foreach (ListItem item in lbMembers.Items)
            {
                if (item.Value == name)
                {
                    return;
                }
                else
                {
                    count += 1;
                }
            }
            if (count == lbMembers.Items.Count)
            {
                lbMembers.Items.Add(name);
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (txtTitle.Text == "" || lbMembers.Items.Count == 0)
            {
                lblMessage.Text = "Fields Cannot Be Left Blank";
                return;
            }

            string strValues;

            strValues = "title=" + txtTitle.Text;
            strValues += "&desc=" + txtDesc.Text;
            strValues += "&img=" + Server.UrlEncode(imgProjPoster.ImageUrl);
            
            strValues += "&members=";
            for(int i = 0; i < lbMembers.Items.Count; i++)
            {
                strValues += lbMembers.Items[i] + ",";
            }
            strValues = strValues.TrimEnd(',');
            Response.Redirect("ConfirmCreatePP.aspx?" + strValues);
            
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            int selectedIndex = lbMembers.SelectedIndex;
            if(selectedIndex != -1)
            {
                lbMembers.Items.RemoveAt(selectedIndex);
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (fuImg.FileName == "")
            {
                return;
            }
            string savePath = MapPath("../Assets/ProjectImages/" + fuImg.FileName);

            fuImg.SaveAs(savePath);

            imgProjPoster.ImageUrl = "../Assets/ProjectImages/" + fuImg.FileName;
        }
    }
}