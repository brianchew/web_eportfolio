﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using WEB_ePortfolio.Classes;


namespace WEB_ePortfolio.Student
{
    public partial class Update : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string email = Session["LoginID"].ToString();
                StudentClass stud = new StudentClass();
                stud.Email = email;
                int errCode = stud.getDetails();
                if (errCode == 0)
                {
                    txtAchievements.Text = stud.Achievement;
                    txtSelfDesc.Text = stud.Description;
                }

                string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("SELECT SkillSetName FROM SkillSet", conn);
                SqlDataAdapter daStud = new SqlDataAdapter(cmd);
                DataSet result = new DataSet();

                conn.Open();
                daStud.Fill(result, "StudDetails");
                conn.Close();

                for (int i = 0; i < result.Tables["StudDetails"].Rows.Count; i++)
                {
                    cblSkillSets.Items.Add(result.Tables["StudDetails"].Rows[i]["SkillSetName"].ToString());
                }

                strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
                conn = new SqlConnection(strConn);
                cmd = new SqlCommand("SELECT SkillSetName FROM SkillSet WHERE SkillSetID IN (SELECT SkillSetID FROM StudentSkillSet WHERE StudentID=@studentid)", conn);
                cmd.Parameters.AddWithValue("@studentid", stud.StudentID);
                daStud = new SqlDataAdapter(cmd);
                result = new DataSet();

                conn.Open();
                daStud.Fill(result, "StudDetails");
                conn.Close();

                foreach (ListItem item in cblSkillSets.Items)
                {
                    for (int i = 0; i < result.Tables["StudDetails"].Rows.Count; i++)
                    {
                        if (item.Text == result.Tables["StudDetails"].Rows[i]["SkillSetName"].ToString())
                        {
                            item.Selected = true;
                        }
                    }
                }
            }
            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtAchievements.Text == "" || txtSelfDesc.Text == "")
            {
                lblMsg.Text = "Fields cannot be left blank";
            }
            string strValues = "";

            strValues += "achievements=" + txtAchievements.Text;
            strValues += "&selfdesc=" + txtSelfDesc.Text;
            strValues += "&skillsets=";

            for(int i = 0; i < cblSkillSets.Items.Count; i++)
            {
                if (cblSkillSets.Items[i].Selected)
                {
                    strValues += cblSkillSets.Items[i].Text + ",";
                }
            }

            strValues = strValues.TrimEnd(',');
            Response.Redirect("UpdateConfirm.aspx?" + strValues);
        }
    }
}