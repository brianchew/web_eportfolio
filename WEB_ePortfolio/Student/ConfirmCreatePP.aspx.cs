﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using WEB_ePortfolio.Classes;

namespace WEB_ePortfolio.Student
{
    public partial class ConfirmCreatePP : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string title = Request.QueryString["title"];
                string desc = Request.QueryString["desc"];

                lblTitle.Text = title;
                txtDesc.Text = desc;

                string imgName = Server.UrlDecode(Request.QueryString["img"]);
                imgProjPoster.ImageUrl = imgName;

                string members = Request.QueryString["members"];
                string[] membersList = members.Split(',');
                for (int i = 0; i < membersList.Count(); i++)
                {
                    blMember.Items.Add(membersList[i]);
                }
                

            }

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("CreatePP.aspx");
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            Project proj = new Project();
            proj.Title = lblTitle.Text;
            proj.Description = txtDesc.Text;
            proj.ProjectID = proj.getNewID();
            string imgName = Server.UrlDecode(Request.QueryString["img"]);
            imgName = imgName.Replace("../Assets/ProjectImages/", "");
            proj.ProjectPoster = imgName;
            proj.add();

            //member stuff
            List<int> memberID = new List<int>();
            string members = Request.QueryString["members"];
            string[] membersList = members.Split(',');
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            for (int i = 0; i < membersList.Count(); i++)
            {
                SqlCommand cmd = new SqlCommand("SELECT StudentID FROM Student WHERE Name=@name", conn);
                cmd.Parameters.AddWithValue("@name", membersList[i]);
                SqlDataAdapter daMem = new SqlDataAdapter(cmd);
                DataSet result = new DataSet();
                conn.Open();
                daMem.Fill(result, "MemberDetails");
                conn.Close();
                memberID.Add(Convert.ToInt16(result.Tables["MemberDetails"].Rows[0]["StudentID"]));
            }

            StudentClass stud = new StudentClass();
            stud.Email = Session["LoginID"].ToString();
            stud.getDetails();

            SqlCommand cmd2 = new SqlCommand("INSERT INTO ProjectMember(ProjectID, StudentID, Role) VALUES(@projectid, @studentid, 'Leader')", conn);
            cmd2.Parameters.AddWithValue("@projectid", proj.ProjectID);
            cmd2.Parameters.AddWithValue("@studentid", stud.StudentID);
            conn.Open();
            cmd2.ExecuteNonQuery();
            conn.Close();
            foreach (int id in memberID)
            {
                SqlCommand cmd3 = new SqlCommand("INSERT INTO ProjectMember(ProjectID, StudentID, Role) VALUES(@projectid, @studentid, 'Member')", conn);
                cmd3.Parameters.AddWithValue("@projectid", proj.ProjectID);
                cmd3.Parameters.AddWithValue("@studentid", id);
                conn.Open();
                cmd3.ExecuteNonQuery();
                conn.Close();
            }

            Response.Redirect("Index.aspx");
        }
    }
}