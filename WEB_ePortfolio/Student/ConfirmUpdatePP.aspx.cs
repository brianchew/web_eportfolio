﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using WEB_ePortfolio.Classes;

namespace WEB_ePortfolio.Student
{
    public partial class ConfirmUpdatePP : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string title = Server.UrlDecode(Request.QueryString["title"]);
                string desc = Server.UrlDecode(Request.QueryString["desc"]);

                lblTitle.Text = title;
                txtDesc.Text = desc;

                imgProjPoster.ImageUrl = Server.UrlDecode(Request.QueryString["img"]);
                txtReflection.Text = Server.UrlDecode(Request.QueryString["reflection"]);

                string members = Request.QueryString["members"];
                string[] membersList = members.Split(',');
                for (int i = 0; i < membersList.Count(); i++)
                {
                    blMember.Items.Add(membersList[i]);
                }
            }

        }

        protected void btnComplete_Click(object sender, EventArgs e)
        {
            Project proj = new Project();
            proj.ProjectID = Convert.ToInt16(Session["ProjID"]);
            
            StudentClass stud = new StudentClass();
            stud.Email = Session["LoginID"].ToString();
            stud.getDetails();

            if (Convert.ToInt16(Session["Leader"]) == 1)
            {
                string title = Server.UrlDecode(Request.QueryString["title"]);
                string desc = Server.UrlDecode(Request.QueryString["desc"]);
                proj.Title = title;
                proj.Description = desc;
                string imgName = imgProjPoster.ImageUrl;
                imgName = imgName.Replace("../Assets/ProjectImages/", "");
                proj.ProjectPoster = imgName;
                int errCode = proj.update();
                if (errCode == -1)
                {
                    return;
                }
            }

            string reflection = Server.UrlDecode(Request.QueryString["reflection"]).ToString();

            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("UPDATE ProjectMember SET Reflection=@reflection WHERE StudentID=@studentid AND ProjectID=@projectid", conn);
            cmd.Parameters.AddWithValue("@reflection", reflection);
            cmd.Parameters.AddWithValue("@studentid", stud.StudentID);
            cmd.Parameters.AddWithValue("@projectid", proj.ProjectID);

            conn.Open();
            int count = cmd.ExecuteNonQuery();

            conn.Close();

            Response.Redirect("Index.aspx");
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("UpdatePP.aspx");
        }
    }
}