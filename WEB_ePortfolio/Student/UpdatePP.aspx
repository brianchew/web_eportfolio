﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/Student.Master" AutoEventWireup="true" CodeBehind="UpdatePP.aspx.cs" Inherits="WEB_ePortfolio.Student.UpdatePP
    " %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style1 {
        height: 26px;
    }
    .auto-style2 {
            width: 201px;
        }
    .auto-style3 {
            height: 26px;
            width: 201px;
        }
        .auto-style5 {
            height: 16px;
            width: 201px;
        }
        .auto-style6 {
            height: 16px;
        }
        .auto-style7 {
            width: 201px;
            height: 101px;
        }
        .auto-style8 {
            text-align: left;
            height: 101px;
        }
        .auto-style9 {
            width: 282px;
        }
        .auto-style10 {
            height: 16px;
            width: 282px;
        }
        .auto-style11 {
            height: 26px;
            width: 282px;
        }
        .auto-style12 {
            text-align: left;
            height: 101px;
            width: 282px;
        }
        .auto-style13 {
            width: 282px;
            text-align: left;
            height: 33px;
        }
        .auto-style14 {
            width: 201px;
            height: 33px;
        }
        .auto-style15 {
            height: 33px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid h-100 bg-secondary">
        <div class="container shadow-lg mb-5 p-5 card text-white bg-secondary">
            <table class="w-100">
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style9">Update Project Portfolio</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="lblSelProj" runat="server" Text="Select Project Portfolio:"></asp:Label>
                    </td>
                    <td class="auto-style10">
                        <asp:DropDownList ID="ddlSelProj" runat="server" Width="250px" CssClass="form-control">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style6">
                        <asp:Button ID="btnLoad" runat="server" CssClass="form-control" OnClick="btnLoad_Click" Text="Load" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        </td>
                    <td class="auto-style10">
                        <asp:Label ID="lblLeader" runat="server"></asp:Label>
                    </td>
                    <td class="auto-style6">
                        </td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="lblProjTitle" runat="server" Text="Edit Project Title:"></asp:Label>
                    </td>
                    <td class="auto-style10">
                        <asp:TextBox ID="txtTitle" runat="server" Width="245px" CssClass="form-control"></asp:TextBox>
                    </td>
                    <td class="auto-style6">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="lblProjDesc" runat="server" Text="Edit Project Description:"></asp:Label>
                    </td>
                    <td class="auto-style11">
                        <asp:TextBox ID="txtDesc" runat="server" Height="100px" TextMode="MultiLine" Width="250px" CssClass="form-control"></asp:TextBox>
                    </td>
                    <td class="auto-style1">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style7">
                        <asp:Label ID="lblAddStud" runat="server" Text="Add Student:"></asp:Label>
                    </td>
                    <td class="auto-style12">
                        <asp:Label ID="lblName" runat="server" Text="Name:" Width="100px"></asp:Label>
                        <asp:DropDownList ID="ddlName" runat="server" Width="200px" CssClass="form-control">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style8">
                        <asp:Button ID="btnAdd" runat="server" Width="100px" OnClick="btnAdd_Click" Text="Add" CssClass="form-control" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="lblStudList" runat="server" Text="Current Students:"></asp:Label>
                    </td>
                    <td class="auto-style11">
                        <asp:ListBox ID="lbMembers" runat="server" Width="250px" CssClass="form-control"></asp:ListBox>
                        <asp:Label ID="lblLeadRemove" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td class="auto-style1">
                        <asp:Button ID="btnRemove" runat="server" OnClick="btnRemove_Click" Text="Remove" CssClass="form-control" Width="100px"/>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style9">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style16">
                        <asp:Label ID="lblProjPost" runat="server" Text="Upload Project Poster:"></asp:Label>
                    </td>
                    <td class="auto-style17">
                        <asp:Image ID="imgProjPoster" runat="server" Height="500px" Width="500px" />
                        <asp:FileUpload ID="fuImg" runat="server" CssClass="form-control" />
                    </td>
                    <td class="auto-style18">
                        <asp:Button ID="btnUpload" runat="server" CssClass="form-control" OnClick="btnUpload_Click" Text="Upload" Width="100px" />
                        </td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style9">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lblReflection" runat="server" Text="Reflection:"></asp:Label>
                    </td>
                    <td class="auto-style9">
                        <asp:TextBox ID="txtReflection" runat="server" CssClass="form-control" Height="100px" TextMode="MultiLine" Width="250px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style9">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style9">
                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style14"></td>
                    <td class="auto-style13">
                        <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" CssClass="form-control" Width="250px"/>
                    </td>
                    <td class="auto-style15"></td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
