﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/Student.Master" AutoEventWireup="true" CodeBehind="ConfirmUpdatePP.aspx.cs" Inherits="WEB_ePortfolio.Student.ConfirmUpdatePP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 26px;
        }
        .auto-style2 {
            height: 26px;
            width: 200px;
        }
        .auto-style3 {
            width: 200px;
        }
        .auto-style4 {
            height: 27px;
            width: 200px;
        }
        .auto-style6 {
            width: 200px;
            height: 5px;
        }
        .auto-style7 {
            height: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid h-100 bg-secondary">
        <div class="container shadow-lg mb-5 p-5 card text-white bg-secondary">
            <table class="w-100">
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style1">The Following Project Portfolio Has Been Created</td>
                </tr>
                <tr>
                    <td class="auto-style4"></td>
                    <td class="auto-style1">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">Project Title:</td>
                    <td class="auto-style1">
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">Project Description:</td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtDesc" runat="server" Enabled="False" Height="100px" TextMode="MultiLine" Width="250px" CssClass="form-control"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">Members:</td>
                    <td>
                        <asp:BulletedList ID="blMember" runat="server">
                        </asp:BulletedList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">Project Poster:</td>
                    <td>
                        <asp:Image ID="imgProjPoster" runat="server" Height="500px" Width="500px" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style6"></td>
                    <td class="auto-style7">
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">Reflection:</td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtReflection" runat="server" Enabled="False" Height="100px" TextMode="MultiLine" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" CssClass="form-control" Width="100px"/>
                        <br />
                        <asp:Button ID="btnComplete" runat="server" Text="Complete" OnClick="btnComplete_Click" CssClass="form-control" Width="100px"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
