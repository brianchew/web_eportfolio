﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/Student.Master" AutoEventWireup="true" CodeBehind="Update.aspx.cs" Inherits="WEB_ePortfolio.Student.Update" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 140px;
        }
        .auto-style2 {
            width: 140px;
            height: 26px;
        }
        .auto-style3 {
            height: 26px;
        }
        .auto-style4 {
            width: 140px;
            height: 48px;
        }
        .auto-style5 {
            height: 48px;
        }
        .auto-style6 {
            width: 140px;
            height: 28px;
        }
        .auto-style7 {
            height: 28px;
        }
        .auto-style8 {
            display: block;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            background-clip: padding-box;
            border-radius: .25rem;
            transition: none;
            border: 1px solid #ced4da;
            background-color: #fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid h-100 bg-secondary">
        <div class="container shadow-lg mb-5 p-5 card text-white bg-secondary">
            <table class="w-100">
                <tr>
                    <td class="auto-style2"></td>
                    <td class="auto-style3">Update Personal Portfolio</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lblAchievements" runat="server" Text="Achievements:"></asp:Label>
                    </td>
                    <td class="auto-style3">
                        <asp:TextBox ID="txtAchievements" runat="server" Height="100px" TextMode="MultiLine" Width="275px" CssClass="form-control"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="lblSelfDesc" runat="server" Text="Self-Description:"></asp:Label>
                    </td>
                    <td class="auto-style5">
                        <asp:TextBox ID="txtSelfDesc" runat="server" Height="100px" TextMode="MultiLine" Width="275px" CssClass="form-control"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style6">
                        <asp:Label ID="lblSkillSets" runat="server" Text="Skill-Sets:"></asp:Label>
                    </td>
                    <td class="auto-style7">
                        <asp:CheckBoxList ID="cblSkillSets" runat="server">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style6">
                        &nbsp;</td>
                    <td class="auto-style7">
                        <asp:Label ID="lblMsg" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td>
                        <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" CssClass="auto-style8" Width="275px" />
                    </td>
                </tr>
            </table>
        </div>
        
    </div>
</asp:Content>
