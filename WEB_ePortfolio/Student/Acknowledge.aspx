﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/Student.Master" AutoEventWireup="true" CodeBehind="Acknowledge.aspx.cs" Inherits="WEB_ePortfolio.Student.Acknowledge" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 200px;
        }
        .auto-style2 {
            width: 200px;
            height: 158px;
        }
        .auto-style3 {
            height: 158px;
        }
        .auto-style5 {
            width: 200px;
            height: 40px;
        }
        .auto-style6 {
            height: 40px;
        }
        .auto-style7 {
            width: 263px;
        }
        .auto-style8 {
            height: 40px;
            width: 263px;
        }
        .auto-style9 {
            height: 158px;
            width: 263px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid h-100 bg-secondary">
        <div class="container shadow-lg mb-5 p-5 card text-white bg-secondary">
            
            <table class="w-100">
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td class="auto-style7">Acknowledge Suggestions</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="lblSuggestion" runat="server" Text="Suggestion ID:"></asp:Label>
                    </td>
                    <td class="auto-style7">
                        <asp:DropDownList ID="ddlSuggestion" runat="server" CssClass="form-control" Width="250px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnView" runat="server" CssClass="form-control" OnClick="btnView_Click" Text="View Suggestion" Width="150px" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="lblMentorNameTxt" runat="server" Text="Mentor Name:"></asp:Label>
                    </td>
                    <td class="auto-style8">
                        <asp:Label ID="lblMentorName" runat="server"></asp:Label>
                    </td>
                    <td class="auto-style6">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lblSuggestionContent" runat="server" Text="Suggestion Content:"></asp:Label>
                    </td>
                    <td class="auto-style9">
                        <asp:TextBox ID="txtSuggestion" runat="server" Enabled="False" Height="150px" Width="250px" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    <td class="auto-style3">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td class="auto-style7">
                        <asp:Button ID="btnAcknowledge" runat="server" Text="Acknowledge" CssClass="form-control" OnClick="btnAcknowledge_Click" Width="250px"/>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
            
        </div>
    </div>
</asp:Content>
