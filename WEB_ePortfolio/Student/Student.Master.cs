﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WEB_ePortfolio.Classes;

namespace WEB_ePortfolio.Student
{
    public partial class Student : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LoginID"] != null)
            {
                
            }
            else
            {
                Response.Redirect("../Login.aspx");
            }
        }
    }
}