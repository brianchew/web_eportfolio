﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_ePortfolio.Student
{
    public partial class ConfirmAcknowledge : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblSuggestionID.Text = Session["SuggID"].ToString();
                lblMentorName.Text = Request.QueryString["mentorName"].ToString();
                txtSuggestion.Text = Request.QueryString["suggestion"].ToString();
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx");
        }
    }
}