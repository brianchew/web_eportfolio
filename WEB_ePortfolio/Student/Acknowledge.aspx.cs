﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using WEB_ePortfolio.Classes;

namespace WEB_ePortfolio.Student
{
    public partial class Acknowledge : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string email = Session["LoginID"].ToString();
                StudentClass stud = new StudentClass();
                stud.Email = email;
                int errCode = stud.getDetails();

                string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("SELECT SuggestionID FROM Suggestion WHERE StudentID=@studentid AND Status='N'", conn);
                cmd.Parameters.AddWithValue("@studentid", stud.StudentID);
                SqlDataAdapter daSugg = new SqlDataAdapter(cmd);
                DataSet result = new DataSet();

                conn.Open();
                daSugg.Fill(result, "SuggDetails");
                conn.Close();

                for (int i = 0; i < result.Tables["SuggDetails"].Rows.Count; i++)
                {
                    ddlSuggestion.Items.Add(result.Tables["SuggDetails"].Rows[0]["SuggestionID"].ToString());
                }
            }
        }

        protected void btnAcknowledge_Click(object sender, EventArgs e)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("UPDATE Suggestion SET Status='Y' WHERE SuggestionID=@suggid", conn);
            cmd.Parameters.AddWithValue("@suggid", Session["SuggID"]);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            string strValues = "";
            strValues += "mentorName=" + lblMentorName.Text;
            strValues += "&suggestion=" + txtSuggestion.Text;
            Response.Redirect("ConfirmAcknowledge.aspx?" + strValues);
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            if (ddlSuggestion.SelectedIndex == -1)
            {
                return;
            }
            int suggID = Convert.ToInt16(ddlSuggestion.SelectedValue);
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT MentorID, Description FROM Suggestion WHERE SuggestionID=@suggestionid", conn);
            cmd.Parameters.AddWithValue("@suggestionid", suggID);

            SqlDataAdapter daSugg = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daSugg.Fill(result, "SuggDetails");
            conn.Close();

            txtSuggestion.Text = result.Tables["SuggDetails"].Rows[0]["Description"].ToString();

            cmd = new SqlCommand("SELECT Name FROM Mentor WHERE MentorID=@mentorid", conn);
            cmd.Parameters.AddWithValue("@mentorid", Convert.ToInt16(result.Tables["SuggDetails"].Rows[0]["MentorID"]));

            daSugg = new SqlDataAdapter(cmd);
            result = new DataSet();

            conn.Open();
            daSugg.Fill(result, "MentorDetails");
            conn.Close();

            lblMentorName.Text = result.Tables["MentorDetails"].Rows[0]["Name"].ToString();
            Session["SuggID"] = suggID;
        }
    }
}