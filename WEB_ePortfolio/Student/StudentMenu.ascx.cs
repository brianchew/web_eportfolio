﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WEB_ePortfolio.Classes;

namespace WEB_ePortfolio.Student
{
    public partial class StudentMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string email = Session["LoginID"].ToString();
                StudentClass stud = new StudentClass();
                stud.Email = email;
                int errCode = stud.getDetails();
                if (errCode == 0)
                {
                    lblUser.Text = "Welcome, " + stud.Name;
                }
            }
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("../Login.aspx");
        }
    }
}