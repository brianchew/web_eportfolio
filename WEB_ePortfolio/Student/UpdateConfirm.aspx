﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/Student.Master" AutoEventWireup="true" CodeBehind="UpdateConfirm.aspx.cs" Inherits="WEB_ePortfolio.Student.UpdateConfirm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 140px;
        }
        .auto-style2 {
            display: block;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            background-clip: padding-box;
            border-radius: .25rem;
            transition: none;
            border: 1px solid #ced4da;
            background-color: #fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid h-100 bg-secondary">
        <div class="container shadow-lg mb-5 p-5 card text-white bg-secondary">

            <table class="w-100">
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td>Confirm Update?</td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="lblAchTitle" runat="server" Text="Achievements:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAchievements" runat="server" Enabled="False" Height="150px" TextMode="MultiLine" Width="350px" CssClass="form-control"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="lblSelfDescTitle" runat="server" Text="Self-Description"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSelfDesc" runat="server" Enabled="False" Height="150px" TextMode="MultiLine" Width="350px" CssClass="form-control"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="lblSkillSet" runat="server" Text="Skill-Sets:"></asp:Label>
                    </td>
                    <td>
                        <asp:BulletedList ID="blSkillSets" runat="server">
                        </asp:BulletedList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td>
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="auto-style2" OnClick="btnBack_Click" Width="100px"/>
                        <br />
&nbsp;<asp:Button ID="btnConfirm" runat="server" Text="Confirm" CssClass="auto-style2" OnClick="btnConfirm_Click" Width="100px"/>
                    </td>
                </tr>
            </table>

        </div>
    </div>
</asp:Content>
