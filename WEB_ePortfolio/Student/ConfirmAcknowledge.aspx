﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/Student.Master" AutoEventWireup="true" CodeBehind="ConfirmAcknowledge.aspx.cs" Inherits="WEB_ePortfolio.Student.ConfirmAcknowledge" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 181px;
        }
        .auto-style2 {
            width: 181px;
            height: 40px;
        }
        .auto-style3 {
            height: 27px;
        }
        .auto-style4 {
            height: 40px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid h-100 bg-secondary">
        <div class="container shadow-lg mb-5 p-5 card text-white bg-secondary">
            
            <table class="w-100">
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td>Suggestion Acknoledged!</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lblSuggestion" runat="server" Text="Suggestion ID:"></asp:Label>
                    </td>
                    <td class="auto-style4">
                        <asp:Label ID="lblSuggestionID" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="lblMentorNameTxt" runat="server" Text="Mentor Name:"></asp:Label>
                    </td>
                    <td class="auto-style4">
                        <asp:Label ID="lblMentorName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="lblSuggestionContent" runat="server" Text="Suggestion Content:"></asp:Label>
                    </td>
                    <td class="auto-style3">
                        <asp:TextBox ID="txtSuggestion" runat="server" Enabled="False" Height="150px" Width="250px" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td>
                        <asp:Button ID="btnReturn" runat="server" Text="Return" CssClass="form-control" Width="250px" OnClick="btnReturn_Click"/>
                    </td>
                </tr>
            </table>
            
        </div>
    </div>
</asp:Content>
