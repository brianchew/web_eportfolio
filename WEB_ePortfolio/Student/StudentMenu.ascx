﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StudentMenu.ascx.cs" Inherits="WEB_ePortfolio.Student.StudentMenu" %>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
    <a class="navbar-brand" href="Main.aspx" style="font-size:32px; font-weight:bold; color:white;">
        ABC Polytechnic
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#staffNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="staffNavbar">
        <ul class="navbar-nav mr-auto ">
            <li class="nav-item text-white">
                <a class="nav-link" href="Index.aspx">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Update.aspx">Update Personal Portfolio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="CreatePP.aspx">Create Project Portfolio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="UpdatePP.aspx">Update Project Portfolio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Acknowledge.aspx">Acknowledge Suggestions</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <asp:Label ID="lblUser" runat="server" ForeColor="White" Width="250px"></asp:Label>
            <asp:Button ID="btnLogOut" runat="server" Text="Log Out" OnClick="btnLogOut_Click" style="height: 26px" />
        </ul>
    </div>
</nav>