﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using WEB_ePortfolio.Classes;

namespace WEB_ePortfolio.Student
{
    public partial class UpdatePP : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string email = Session["LoginID"].ToString();
                StudentClass stud = new StudentClass();
                stud.Email = email;
                stud.getDetails();

                string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("SELECT Name FROM Student WHERE EmailAddr!=@emailaddr", conn);
                cmd.Parameters.AddWithValue("@emailaddr", stud.Email);
                SqlDataAdapter daStud = new SqlDataAdapter(cmd);
                DataSet result = new DataSet();

                conn.Open();
                daStud.Fill(result, "StudDetails");
                conn.Close();

                for (int i = 0; i < result.Tables["StudDetails"].Rows.Count; i++)
                {
                    ddlName.Items.Add(result.Tables["StudDetails"].Rows[i]["Name"].ToString());
                }

                cmd = new SqlCommand("SELECT Title FROM Project WHERE ProjectID IN (SELECT ProjectID FROM ProjectMember WHERE StudentID=@studentid)", conn);
                cmd.Parameters.AddWithValue("@studentid", stud.StudentID);
                SqlDataAdapter daProj = new SqlDataAdapter(cmd);
                result = new DataSet();

                conn.Open();
                daProj.Fill(result, "ProjDetails");
                conn.Close();
                
                for (int i = 0; i < result.Tables["ProjDetails"].Rows.Count; i++)
                {
                    ddlSelProj.Items.Add(result.Tables["ProjDetails"].Rows[i]["Title"].ToString());
                }

            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            string name = ddlName.SelectedValue;

            int count = 0;
            foreach (ListItem item in lbMembers.Items)
            {
                if (item.Value == name)
                {
                    return;
                }
                else
                {
                    count += 1;
                }
            }
            if (count == lbMembers.Items.Count)
            {
                lbMembers.Items.Add(name);
            }
        }

        

        

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (fuImg.FileName == "")
            {
                return;
            }
            string savePath = MapPath("../Assets/ProjectImages/" + fuImg.FileName);

            fuImg.SaveAs(savePath);

            imgProjPoster.ImageUrl = "../Assets/ProjectImages/" + fuImg.FileName;
        }


        protected void btnLoad_Click(object sender, EventArgs e)
        {
            btnAdd.Enabled = true;
            btnRemove.Enabled = true;
            btnUpload.Enabled = true;
            ddlName.Enabled = true;
            fuImg.Enabled = true;
            txtDesc.Enabled = true;
            txtTitle.Enabled = true;
            lblLeader.Text = "";

            txtTitle.Text = "";
            txtDesc.Text = "";
            lbMembers.Items.Clear();
            Project proj = new Project();
            proj.Title = ddlSelProj.SelectedValue;
            int errCode = proj.getDetails();
            if (errCode == -1)
            {
                return;
            }
            Session["ProjID"] = proj.ProjectID;
            txtTitle.Text = proj.Title;
            txtDesc.Text = proj.Description;
            imgProjPoster.ImageUrl = "../Assets/ProjectImages/" + proj.ProjectPoster;

            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT Name FROM Student WHERE StudentID IN (SELECT StudentID FROM ProjectMember WHERE ProjectID=@projectid)", conn);
            cmd.Parameters.AddWithValue("@projectid", proj.ProjectID);
            SqlDataAdapter daProj = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daProj.Fill(result, "ProjDetails");
            conn.Close();
            for (int i = 0; i < result.Tables["ProjDetails"].Rows.Count; i++)
            {
                lbMembers.Items.Add(result.Tables["ProjDetails"].Rows[i]["Name"].ToString());
            }


            string email = Session["LoginID"].ToString();
            StudentClass stud = new StudentClass();
            stud.Email = email;
            stud.getDetails();

            cmd = new SqlCommand("SELECT Reflection FROM ProjectMember WHERE StudentID=@studentid AND ProjectID=@projectid", conn);
            cmd.Parameters.AddWithValue("@studentid", stud.StudentID);
            cmd.Parameters.AddWithValue("@projectid", proj.ProjectID);

            daProj = new SqlDataAdapter(cmd);
            result = new DataSet();

            conn.Open();
            daProj.Fill(result, "Reflection");
            conn.Close();

            txtReflection.Text = result.Tables["Reflection"].Rows[0]["Reflection"].ToString();


            cmd = new SqlCommand("SELECT Role FROM ProjectMember WHERE StudentID=@studentid AND ProjectID=@projectid", conn);
            cmd.Parameters.AddWithValue("@studentid", stud.StudentID);
            cmd.Parameters.AddWithValue("@projectid", proj.ProjectID);

            daProj = new SqlDataAdapter(cmd);
            result = new DataSet();

            conn.Open();
            daProj.Fill(result, "MemberRole");
            conn.Close();

            if (result.Tables["MemberRole"].Rows[0]["Role"].ToString() != "Leader")
            {
                Session["Lead"] = 0;
                btnAdd.Enabled = false;
                btnRemove.Enabled = false;
                btnUpload.Enabled = false;
                ddlName.Enabled = false;
                fuImg.Enabled = false;
                txtDesc.Enabled = false;
                txtTitle.Enabled = false;
                lblLeader.Text = "You Are Not The Leader Of This Project";
            }
            else
            {
                Session["Leader"] = 1;
                Session["LeaderName"] = stud.Name;
            }
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            int selectedIndex = lbMembers.SelectedIndex;
            if (lbMembers.Items[selectedIndex].Text == Session["LeaderName"].ToString())
            {
                lblLeadRemove.Text = "You Cannot Remove Yourself From The Project";
            }
            if (selectedIndex != -1 && lbMembers.Items[selectedIndex].Text != Session["LeaderName"].ToString())
            {
                lbMembers.Items.RemoveAt(selectedIndex);
            }

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtTitle.Text == "" && txtDesc.Text == "" && lbMembers.Items.Count == 0)
            {
                lblMessage.Text = "No Project Portfolio Loaded";
                return;
            }
            if (txtTitle.Text == "" || txtDesc.Text == "" || lbMembers.Items.Count == 0)
            {
                lblMessage.Text = "Fields Cannot Be Left Blank";
                return;
            }

            string strValues;
            strValues = "title=" + Server.UrlEncode(txtTitle.Text);
            strValues += "&desc=" + Server.UrlEncode(txtDesc.Text);


            strValues += "&members=";
            for (int i = 0; i < lbMembers.Items.Count; i++)
            {
                strValues += lbMembers.Items[i] + ",";
            }
            strValues = strValues.TrimEnd(',');

            strValues += "&img=" + Server.UrlEncode(imgProjPoster.ImageUrl);
            strValues += "&reflection=" + Server.UrlEncode(txtReflection.Text);

            Response.Redirect("ConfirmUpdatePP.aspx?" + strValues);

        }
    }
}