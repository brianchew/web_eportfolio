﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/Student.Master" AutoEventWireup="true" CodeBehind="CreatePP.aspx.cs" Inherits="WEB_ePortfolio.Student.CreatePP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style1 {
        height: 26px;
    }
    .auto-style2 {
            width: 162px;
        }
    .auto-style3 {
            height: 26px;
            width: 162px;
        }
        .auto-style4 {
            display: block;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            background-clip: padding-box;
            border-radius: .25rem;
            transition: none;
            border: 1px solid #ced4da;
            background-color: #fff;
        }
        .auto-style5 {
            height: 16px;
            width: 162px;
        }
        .auto-style6 {
            height: 16px;
        }
        .auto-style7 {
            width: 162px;
            height: 101px;
        }
        .auto-style8 {
            text-align: left;
            height: 101px;
        }
        .auto-style9 {
            width: 289px;
        }
        .auto-style10 {
            height: 16px;
            width: 289px;
        }
        .auto-style11 {
            height: 26px;
            width: 289px;
        }
        .auto-style12 {
            text-align: left;
            height: 101px;
            width: 289px;
        }
        .auto-style13 {
            width: 289px;
            text-align: left;
            height: 33px;
        }
        .auto-style14 {
            width: 162px;
            height: 33px;
        }
        .auto-style15 {
            height: 33px;
        }
        .auto-style16 {
            height: 25px;
            width: 162px;
        }
        .auto-style17 {
            height: 25px;
            width: 289px;
        }
        .auto-style18 {
            height: 25px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid h-100 bg-secondary">
        <div class="container shadow-lg mb-5 p-5 card text-white bg-secondary">
            <table class="w-100">
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style9">Create Project Portfolio</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="lblProjTitle" runat="server" Text="Project Title:"></asp:Label>
                    </td>
                    <td class="auto-style10">
                        <asp:TextBox ID="txtTitle" runat="server" Width="245px" CssClass="form-control"></asp:TextBox>
                    </td>
                    <td class="auto-style6">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        &nbsp;</td>
                    <td class="auto-style10">
                        &nbsp;</td>
                    <td class="auto-style6">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="lblProjDesc" runat="server" Text="Project Description:"></asp:Label>
                    </td>
                    <td class="auto-style11">
                        <asp:TextBox ID="txtDesc" runat="server" Height="100px" TextMode="MultiLine" Width="250px" CssClass="form-control"></asp:TextBox>
                    </td>
                    <td class="auto-style1">
                        </td>
                </tr>
                <tr>
                    <td class="auto-style7">
                        <asp:Label ID="lblAddStud" runat="server" Text="Add Student:"></asp:Label>
                    </td>
                    <td class="auto-style12">
                        <asp:Label ID="lblName" runat="server" Text="Name:" Width="100px"></asp:Label>
                        <br />
                        <asp:DropDownList ID="ddlName" runat="server" Width="200px" CssClass="form-control">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style8">
                        <asp:Button ID="btnAdd" runat="server" Width="100px" OnClick="btnAdd_Click" Text="Add" CssClass="form-control" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="lblStudList" runat="server" Text="Current Students:"></asp:Label>
                    </td>
                    <td class="auto-style11">
                        <asp:ListBox ID="lbMembers" runat="server" Width="250px" CssClass="auto-style4"></asp:ListBox>
                    </td>
                    <td class="auto-style1">
                        <asp:Button ID="btnRemove" runat="server" OnClick="btnRemove_Click" Text="Remove" CssClass="form-control" Width="100px"/>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style9">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style16">
                        <asp:Label ID="lblProjPost" runat="server" Text="Upload Project Poster:"></asp:Label>
                    </td>
                    <td class="auto-style17">
                        <asp:Image ID="imgProjPoster" runat="server" Height="500px" Width="500px" />
                        <asp:FileUpload ID="fuImg" runat="server" CssClass="form-control" />
                    </td>
                    <td class="auto-style18">
                        <asp:Button ID="btnUpload" runat="server" CssClass="form-control" OnClick="btnUpload_Click" Text="Upload" Width="100px" />
                        </td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style9">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style9">
                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style14"></td>
                    <td class="auto-style13">
                        <asp:Button ID="btnCreate" runat="server" OnClick="btnCreate_Click" Text="Create" CssClass="auto-style4" Width="250px"/>
                    </td>
                    <td class="auto-style15"></td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
