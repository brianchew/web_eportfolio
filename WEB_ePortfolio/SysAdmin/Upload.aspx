﻿<%@ Page Title="" Language="C#" MasterPageFile="SysAdmin.Master" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="WEB_ePortfolio.SysAdmin.Upload" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 26px;
        }
        .auto-style2 {
            width: 60px;
        }
        .auto-style3 {
            height: 26px;
            width: 60px;
        }
        .auto-style4 {
            width: 200px;
        }
        .auto-style5 {
            height: 26px;
            width: 200px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="container shadow-lg mb-5 p-5 card text-white bg-secondary" style="left: -15px; top: -2px">
        <br />
        <div class="container shadow-lg h-75">
        <table class="w-100">
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style4">
                    Name of Student</td>
                <td>
                    <asp:DropDownList ID="ddlStu" runat="server" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="Name">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:WEBePortConnectionString %>" SelectCommand="SELECT [Name] FROM [Student]"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style5">Select image to upload</td>
                <td class="auto-style1">
                    <asp:Image ID="StuImg" runat="server" Height="250px" Width="200px" />
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style4">&nbsp;</td>
                <td>
                    <asp:FileUpload ID="UpImg" runat="server" />
                    <asp:Label ID="LblImg" runat="server" Text="LblImg"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style4">&nbsp;</td>
                <td>
                    <asp:Button ID="BtnUpload" runat="server" Text="Upload" Width="125px" OnClick="BtnUpload_Click" />
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style4">
                    <asp:Button ID="BtnConfirm" runat="server" Text="Confirm" Width="125px" OnClick="BtnConfirm_Click" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
            </div>
        </div>
</asp:Content>
