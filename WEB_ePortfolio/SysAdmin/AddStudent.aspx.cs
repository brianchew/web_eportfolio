﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using WEB_ePortfolio.Classes;

namespace WEB_ePortfolio.SysAdmin
{
    public partial class AddStudent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            StudentClass Student = new StudentClass();
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            Student.StudentID = Student.getNewID();
            string SName = txtName.Text;
            string SMail = txtEmail.Text;
            string SCourse = ddlCourse.SelectedValue;
            string SMentor = ddlMentors.SelectedValue;

            
            


            SqlCommand cmd1 = new SqlCommand("Select * From Student Where Name=@name", conn);
            cmd1.Parameters.AddWithValue("@name", SName);
            SqlDataAdapter daStu = new SqlDataAdapter(cmd1);
            DataSet result = new DataSet();
            conn.Open();
            daStu.Fill(result, "StudentDetails");
            conn.Close();
            if (result.Tables["StudentDetails"].Rows.Count > 0)
            {
                lblName.Text = "Name already exist";
            }
            if (SName == "")
            {
                lblName.Text = "Name required";
            }
            

            SqlCommand cmd2 = new SqlCommand("Select * From Student Where EmailAddr=@emailaddr", conn);
            cmd2.Parameters.AddWithValue("@emailaddr", SMail);
            daStu = new SqlDataAdapter(cmd2);
            result = new DataSet();
            conn.Open();
            daStu.Fill(result, "StudentDetails");
            conn.Close();
            if (result.Tables["StudentDetails"].Rows.Count > 0)
            {
                lblEmail.Text = "Email already exist";
                return;
            }
            if (SMail == "")
            {
                lblEmail.Text = "Email Required";
                return;
            }

            SqlCommand cmd4 = new SqlCommand("Select MentorID From Mentor where Name=@name", conn);
            cmd4.Parameters.AddWithValue("@name", SMentor);
            daStu = new SqlDataAdapter(cmd4);
            result = new DataSet();
            conn.Open();
            daStu.Fill(result, "MentorDetails");
            conn.Close();
            Student.MentorID = Convert.ToInt16(result.Tables["MentorDetails"].Rows[0]["MentorID"]);

            Student.Name = SName;
            Student.Email = SMail;
            Student.Course = SCourse;
            Student.add();
            lblCfm.Text = "Student successfully added";
            
        }
    }
}