﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace WEB_ePortfolio.SysAdmin
{
    public partial class Approve : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("UPDATE ViewingRequest SET Status=@status WHERE ViewingRequestID = @viewingrequestid", conn);
            cmd.Parameters.AddWithValue("@viewingrequestid", ddlRequest.SelectedValue);
            cmd.Parameters.AddWithValue("@status", rblApproval.SelectedValue[0]);
            SqlDataAdapter VdUp = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            VdUp.Fill(result, "ViewDetails");
            conn.Close();
        }
    }
}