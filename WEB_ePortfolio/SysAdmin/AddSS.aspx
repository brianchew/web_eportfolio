﻿<%@ Page Title="" Language="C#" MasterPageFile="SysAdmin.Master" AutoEventWireup="true" CodeBehind="AddSS.aspx.cs" Inherits="WEB_ePortfolio.SysAdmin.AddSS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 60px;
        }
        .auto-style2 {
            width: 200px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid h-100 bg-secondary">
        <br />
        <div class="container shadow-lg mb-5 p-5 card text-white bg-secondary">
        <table class="w-100">
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td class="auto-style2">Name of Student</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td class="auto-style2">
                    <asp:Button ID="Button1" runat="server" Text="Confirm" Width="125px" OnClick="Button1_Click" />
                </td>
                <td>
                    <asp:DropDownList ID="ddlStu" runat="server" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="Name">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:WEBePortConnectionString %>" SelectCommand="SELECT DISTINCT [Name] FROM [Student]"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td class="auto-style2">Skill set avaliable</td>
                <td>
                    <asp:CheckBoxList ID="cblSS" runat="server" DataSourceID="StudentSkillSet" DataTextField="SkillSetName" DataValueField="SkillSetName">
                    </asp:CheckBoxList>
                    <asp:SqlDataSource ID="StudentSkillSet" runat="server" ConnectionString="<%$ ConnectionStrings:WEBePortConnectionString %>" SelectCommand="SELECT [SkillSetName] FROM [SkillSet]"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td class="auto-style2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td class="auto-style2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td class="auto-style2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        </div>
    </div>
</asp:Content>
