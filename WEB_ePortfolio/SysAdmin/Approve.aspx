﻿<%@ Page Title="" Language="C#" MasterPageFile="SysAdmin.Master" AutoEventWireup="true" CodeBehind="Approve.aspx.cs" Inherits="WEB_ePortfolio.SysAdmin.Approve" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 60px;
        }
        .auto-style2 {
            width: 200px;
        }
        .auto-style3 {
            width: 60px;
            height: 26px;
        }
        .auto-style4 {
            width: 200px;
            height: 26px;
        }
        .auto-style5 {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid h-100 bg-secondary">
        <br />
        <div class="container shadow-lg mb-5 p-5 card text-white bg-secondary">
                <table class="w-100">
        <tr>
            <td class="auto-style3"></td>
            <td class="auto-style4">Requests</td>
            <td class="auto-style5">
                <asp:DropDownList ID="ddlRequest" runat="server" DataSourceID="SqlDataSource1" DataTextField="ViewingRequestID" DataValueField="ViewingRequestID">
                    <asp:ListItem>Parent1</asp:ListItem>
                    <asp:ListItem>Parent2</asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:WEBePortConnectionString %>" SelectCommand="SELECT [ViewingRequestID] FROM [ViewingRequest] WHERE ([Status] = @Status)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="P" Name="Status" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">Approval</td>
            <td>
                <asp:RadioButtonList ID="rblApproval" runat="server">
                    <asp:ListItem Selected="True">Approve</asp:ListItem>
                    <asp:ListItem>Reject</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td>
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Confirm" Width="100px" />
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
        </div>
    </div>

</asp:Content>
