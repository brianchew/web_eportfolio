﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace WEB_ePortfolio.SysAdmin
{
    public partial class AddMentor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT MAX(MentorID) AS 'ID' FROM Mentor", conn);
            SqlDataAdapter daMen = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daMen.Fill(result, "MenDetails");
            conn.Close();
            int MenID = Convert.ToInt16(result.Tables["MenDetails"].Rows[0]["ID"]);
            MenID += 1;

            string MName = txtName.Text;
            string MMail = txtEmail.Text;
            SqlCommand cmd1 = new SqlCommand("Select * From Mentor Where Name=@name", conn);
            cmd1.Parameters.AddWithValue("@name", MName);
            SqlCommand cmd2 = new SqlCommand("Select * From Mentor Where EmailAddr=@emailaddr", conn);
            cmd2.Parameters.AddWithValue("@emailaddr", MMail);
            daMen = new SqlDataAdapter(cmd1);
            result = new DataSet();
            conn.Open();
            daMen.Fill(result, "MentorDetails");
            conn.Close();
            if (result.Tables["MentorDetails"].Rows.Count > 0)
            {
                lblName.Text = "Name already exist";
            }
            if (MName == "")
            {
                lblName.Text = "Name required";
            }
            daMen = new SqlDataAdapter(cmd2);
            result = new DataSet();
            conn.Open();
            daMen.Fill(result, "MentorDetails");
            conn.Close();
            if (result.Tables["MentorDetails"].Rows.Count > 0)
            {
                lblEmail.Text = "Email already exist";
                return;
            }
            if (MMail == "")
            {
                lblEmail.Text = "Email Required";
                return;
            }
            SqlCommand cmd3 = new SqlCommand("Insert into Mentor (MentorID, Name, EmailAddr, Password) Values (@mentorid, @name, @emailaddr, 'p@55Mentor')", conn);
            SqlCommand cmd4 = new SqlCommand("SET IDENTITY_INSERT Mentor ON", conn);
            SqlCommand cmd5 = new SqlCommand("SET IDENTITY_INSERT Mentor OFF", conn);
            cmd3.Parameters.AddWithValue("@mentorid", MenID);
            cmd3.Parameters.AddWithValue("@name", MName);
            cmd3.Parameters.AddWithValue("@emailaddr", MMail);
            conn.Open();
            cmd3.ExecuteNonQuery();
            cmd4.ExecuteNonQuery();
            cmd5.ExecuteNonQuery();
            conn.Close();
        }
    }
}