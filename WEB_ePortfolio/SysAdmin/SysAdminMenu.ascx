﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SysAdminMenu.ascx.cs" Inherits="WEB_ePortfolio.Mentor.MentorMenu" %>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
    <a class="navbar-brand" href="Main.aspx" style="font-size:32px; font-weight:bold; color:white;">
        ABC Polytechnic
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#staffNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="staffNavbar">
        <ul class="navbar-nav mr-auto ">
            <li class="nav-item text-white">
                <a class="nav-link" href="Index.aspx">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="AddMentor.aspx">Create Mentor Account</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="AddStudent.aspx">Create Student Account</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="AddSS.aspx">Create Skill Sets</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Upload.aspx">Upload Student Photo</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Approve.aspx">Approve Viewing Request</a>
            </li>

        </ul>
        <ul class="navbar-nav ml-auto">
            <asp:Button ID="btnLogOut" runat="server" Text="Log Out" />
        </ul>
    </div>
</nav>