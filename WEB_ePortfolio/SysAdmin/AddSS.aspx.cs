﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace WEB_ePortfolio.SysAdmin
{
    public partial class AddSS : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            /*SqlCommand cmd = new SqlCommand("INSERT into StudentSkillSet (StudentID, SkillSetID) Values((SELECT studentid from student where name = @name), (select skillsetid from skillset where skillsetname=@skillsetname))", conn);
            cmd.Parameters.AddWithValue("@name", ddlStu.SelectedValue);
            cmd.Parameters.AddWithValue("@skillsetname", cblSS.Items[i].Text);*/

            for (int i = 0; i < cblSS.Items.Count; i++)
            {
                if (cblSS.Items[i].Selected)
                {
                    SqlCommand cmd = new SqlCommand("INSERT into StudentSkillSet (StudentID, SkillSetID) Values((SELECT studentid from student where name = @name), (select skillsetid from skillset where skillsetname=@skillsetname))", conn);
                    cmd.Parameters.AddWithValue("@name", ddlStu.SelectedValue);
                    cmd.Parameters.AddWithValue("@skillsetname", cblSS.Items[i].Text);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
    }
}