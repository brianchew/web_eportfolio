﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace WEB_ePortfolio.SysAdmin
{
    public partial class Upload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnUpload_Click(object sender, EventArgs e)
        {
            if (UpImg.FileName == "")
            {
                return;
            }
            string savePath = MapPath("../Assets/StudentImages" + UpImg.FileName);
            UpImg.SaveAs(savePath);
            StuImg.ImageUrl = "../Assets/ProjectImages/" + UpImg.FileName;

        }
        protected void BtnConfirm_Click(object sender, EventArgs e)
        {
            string strValues;
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("UPDATE Student SET Photo=@photo WHERE Name = @name", conn);
            cmd.Parameters.AddWithValue("@name", ddlStu.SelectedValue);
            cmd.Parameters.AddWithValue("@photo", StuImg.ImageUrl);
            SqlDataAdapter daUp = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            strValues = "img=" + Server.UrlEncode(StuImg.ImageUrl);
            conn.Open();
            daUp.Fill(result, "StuDetails");
            conn.Close();
        }
    }
}