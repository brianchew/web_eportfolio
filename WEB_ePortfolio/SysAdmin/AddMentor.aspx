﻿<%@ Page Title="" Language="C#" MasterPageFile="SysAdmin.Master" AutoEventWireup="true" CodeBehind="AddMentor.aspx.cs" Inherits="WEB_ePortfolio.SysAdmin.AddMentor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 60px;
        }
        .auto-style2 {
            width: 200px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid h-100 bg-secondary">
        <br />
        <div class="container shadow-lg mb-5 p-5 card text-white bg-secondary">
        <table cellpadding="0" cellspacing="0" class="w-100">
            <tr>
                <td>
                    <table class="w-100">
                        <tr>
                            <td class="auto-style1">&nbsp;</td>
                            <td class="auto-style2">Name</td>
                            <td>
                                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                <asp:Label ID="lblName" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1">&nbsp;</td>
                            <td class="auto-style2">Email</td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        </table>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table class="w-100">
                        <tr>
                            <td class="auto-style1">&nbsp;</td>
                            <td class="auto-style2">
                                <asp:Button ID="Button2" runat="server" Text="Confirm" Width="125px" OnClick="Button2_Click" />
                                <asp:Label ID="lblCfm" runat="server"></asp:Label>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        </div>
    </div>
</asp:Content>
