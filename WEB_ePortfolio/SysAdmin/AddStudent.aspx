﻿<%@ Page Title="" Language="C#" MasterPageFile="SysAdmin.Master" AutoEventWireup="true" CodeBehind="AddStudent.aspx.cs" Inherits="WEB_ePortfolio.SysAdmin.AddStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 60px;
        }
        .auto-style2 {
            width: 200px;
        }
        .auto-style3 {
            width: 60px;
            height: 27px;
        }
        .auto-style4 {
            width: 200px;
            height: 27px;
        }
        .auto-style5 {
            height: 27px;
        }
        .auto-style6 {
            width: 100%;
            height: 200px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid h-100 bg-secondary">
        <br />
        <div class="container shadow-lg mb-5 p-5 card text-white bg-secondary">
        <table class="auto-style6">
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td class="auto-style2">Name</td>
                <td>
                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                    <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td class="auto-style2">Email</td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                    <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style3"></td>
                <td class="auto-style4">Course</td>
                <td class="auto-style5">
                    <asp:DropDownList ID="ddlCourse" runat="server" DataSourceID="course" DataTextField="Course" DataValueField="Course">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="Course" runat="server" ConnectionString="<%$ ConnectionStrings:Student_EPortfolioConnectionString %>" SelectCommand="SELECT DISTINCT [Course] FROM [Student]"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style4">Mentor</td>
                <td class="auto-style5">
                    <asp:DropDownList ID="ddlMentors" runat="server" DataSourceID="Mentors" DataTextField="Name" DataValueField="Name">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="Mentors" runat="server" ConnectionString="<%$ ConnectionStrings:WEBePortConnectionString %>" SelectCommand="SELECT [Name] FROM [Mentor]"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td class="auto-style2">
                    <asp:Button ID="Button1" runat="server" Text="Confirm" Width="125px" OnClick="Button1_Click" />
                    <asp:Label ID="lblCfm" runat="server"></asp:Label>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
        </div>
    </div>
</asp:Content>
