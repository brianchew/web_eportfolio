﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;

namespace WEB_ePortfolio
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ddlUserType.Items.Add("System Admin");
                ddlUserType.Items.Add("Mentor");
                ddlUserType.Items.Add("Student");
                ddlUserType.Items.Add("Parent");
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string LoginID = txtEmail.Text;
            string password = txtPassword.Text;
            string userType = ddlUserType.SelectedValue;
            //For session stuff
            string loginIDLowercase = txtEmail.Text.ToLower();
            

            if (LoginID == "" || password == "")
            {
                lblErrorMsg.Text = "Invalid credentials. Your details have been logged.";
                return;
            }

            if (LoginID == "admin@ap.edu.sg" && password == "passAdmin" && userType == "System Admin")
            {
                Session["LoginID"] = loginIDLowercase;
                Session["LoggedInTime"] = DateTime.Now.ToString();
                Response.Redirect("SysAdmin/Index.aspx");

            }
            else if (userType == "Student")
            {
                string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("SELECT Password FROM Student WHERE EmailAddr=@emailaddr", conn);
                cmd.Parameters.AddWithValue("@emailaddr", loginIDLowercase);
                SqlDataAdapter daStud = new SqlDataAdapter(cmd);
                DataSet result = new DataSet();

                conn.Open();
                daStud.Fill(result, "StudDetails");
                conn.Close();

                if (result.Tables["StudDetails"].Rows.Count > 0)
                {
                    if (password == result.Tables["StudDetails"].Rows[0]["Password"].ToString())
                    {
                        Session["LoginID"] = loginIDLowercase;
                        Session["LoggedInTime"] = DateTime.Now.ToString();
                        Response.Redirect("Student/Index.aspx");
                    }
                    else
                    {
                        lblErrorMsg.Text = "Incorrect Email Or Password";
                    }
                }

                else
                {
                    lblErrorMsg.Text = "Incorrect Email Or Password";
                }
            }
            else if (userType == "Mentor")
            {
                string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("SELECT Password FROM Mentor WHERE EmailAddr=@emailaddr", conn);
                cmd.Parameters.AddWithValue("@emailaddr", loginIDLowercase);
                SqlDataAdapter daStud = new SqlDataAdapter(cmd);
                DataSet result = new DataSet();

                conn.Open();
                daStud.Fill(result, "MentorDetails");
                conn.Close();

                if (result.Tables["MentorDetails"].Rows.Count > 0)
                {
                    if (password == result.Tables["MentorDetails"].Rows[0]["Password"].ToString())
                    {
                        int mentorID = getMentorID(txtEmail.Text);
                        Session["LoginID"] = loginIDLowercase;
                        Session["MentorID"] = mentorID;
                        Session["LoggedInTime"] = DateTime.Now.ToString();
                        Response.Redirect("Mentor/Index.aspx");
                    }
                    else
                    {
                        lblErrorMsg.Text = "Incorrect Email Or Password";
                    }
                }

                else
                {
                    lblErrorMsg.Text = "Incorrect Email Or Password";
                }                
            }
            else if (userType == "Parent")
            {
                string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("SELECT Password FROM Parent WHERE EmailAddr=@emailaddr", conn);
                cmd.Parameters.AddWithValue("@emailaddr", loginIDLowercase);
                SqlDataAdapter daStud = new SqlDataAdapter(cmd);
                DataSet result = new DataSet();

                conn.Open();
                daStud.Fill(result, "ParentDetails");
                conn.Close();

                if (result.Tables["ParentDetails"].Rows.Count > 0)
                {
                    if (password == result.Tables["ParentDetails"].Rows[0]["Password"].ToString())
                    {
                        int ParentID = getParentID(txtEmail.Text);
                        Session["ParentID"] = ParentID;
                        Session["LoginID"] = loginIDLowercase;
                        Session["LoggedInTime"] = DateTime.Now.ToString();
                        Response.Redirect("Parent/Index.aspx");
                    }
                    else
                    {
                        lblErrorMsg.Text = "Incorrect Email Or Password";
                    }
                }

                else
                {
                    lblErrorMsg.Text = "Incorrect Email Or Password";
                }
            }
            else
            {
                lblErrorMsg.Text = "Invalid credentials. Your details have been logged.";
                return;
            }

        }
        public int getMentorID(string email)
        {

            //actual
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT MentorID FROM Mentor WHERE EmailAddr=@emailaddr", conn);
            cmd.Parameters.AddWithValue("@emailaddr", email);
            conn.Open();
            int mentorID = Convert.ToInt16(cmd.ExecuteScalar());
            conn.Close();

            //debug
            Debug.WriteLine("Email Address parsed in :" + email);
            Debug.WriteLine("Mentor ID result:" + mentorID);
            return mentorID;
        }
        public int getParentID(string email)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT ParentID FROM Parent WHERE EmailAddr=@emailaddr", conn);
            cmd.Parameters.AddWithValue("@emailaddr", email);
            conn.Open();
            int parentID = Convert.ToInt16(cmd.ExecuteScalar());
            conn.Close();

            //debug
            Debug.WriteLine("Email Address parsed in :" + email);
            Debug.WriteLine("Parent ID result:" + parentID);
            return parentID;


        }
    }
}