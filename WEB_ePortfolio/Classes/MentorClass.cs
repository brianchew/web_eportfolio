﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_ePortfolio.Classes
{
    public class MentorClass
    {
        //Attributes
        private int mentorID;
        private string name;
        private string emailAddr;
        private string password;

        //Properties
        public int MentorID
        {
            get { return mentorID; }
            set { mentorID = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string EmailAddr
        {
            get { return emailAddr; }
            set { emailAddr = value; }
        }
        public string Password
        {
            get { return password; }
            set { password = value; }
        }



    }
}