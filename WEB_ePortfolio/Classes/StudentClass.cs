﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_ePortfolio.Classes
{
    public class StudentClass
    {
        //data types might change later on depending - follow data dict
        private int studentID;
        private string name;
        private string email;
        private string course;
        private string photo; //this might change later on..
        private string description;
        private string achievement;
        private string externalLink;
        private string password;
        private char status;
        private int mentorID;


        public int StudentID
        {
            get { return studentID; }
            set { studentID = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public string Course
        {
            get { return course; }
            set { course = value; }
        }
        public string Photo
        {
            get { return photo; }
            set { photo = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public string Achievement
        {
            get { return achievement; }
            set { achievement = value; }
        }
        public string ExternalLink
        {
            get { return externalLink; }
            set { externalLink = value; }
        }
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        public char Status
        {
            get { return status; }
            set { status = value; }
        }
        public int MentorID
        {
            get { return mentorID; }
            set { mentorID = value; }
        }

        public StudentClass() { }

        public int getDetails()
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE EmailAddr=@emailaddr", conn);
            cmd.Parameters.AddWithValue("@emailaddr", Email);
            SqlDataAdapter daStud = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daStud.Fill(result, "StudDetails");
            conn.Close();

            if (result.Tables["StudDetails"].Rows.Count > 0)
            {
                DataTable table = result.Tables["StudDetails"];
                StudentID = Convert.ToInt16(table.Rows[0]["StudentID"]);
                Name = table.Rows[0]["Name"].ToString();
                Course = table.Rows[0]["Course"].ToString();
                Photo = table.Rows[0]["Photo"].ToString();
                Description = table.Rows[0]["Description"].ToString();
                Achievement = table.Rows[0]["Achievement"].ToString();
                ExternalLink = table.Rows[0]["ExternalLink"].ToString();
                MentorID = Convert.ToInt16(table.Rows[0]["MentorID"]);

                return 0;
            }
            else
            {
                return -1;
            }
        }

        public int update()
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("UPDATE Student SET Achievement=@achievement, Description=@description WHERE StudentID=@studentid", conn);
            cmd.Parameters.AddWithValue("@achievement", Achievement);
            cmd.Parameters.AddWithValue("@description", Description);
            cmd.Parameters.AddWithValue("@studentid", StudentID);

            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();

            if (count > 0)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }


        //MENTOR FUNCTIIONS
        public int loadStudentProjs(ref DataSet result)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT * FROM Project p INNER JOIN ProjectMember pm ON pm.ProjectID=p.ProjectID WHERE StudentID=@selectedStudentID;", conn);

            cmd.Parameters.AddWithValue("@selectedStudentID", StudentID);
            SqlDataAdapter daStudentProjs = new SqlDataAdapter(cmd);

            conn.Open();
            daStudentProjs.Fill(result, "StudentProjDetails");//fill in so can use when datasourcing
            conn.Close();
            return 0;
        }
        public int updateStatus()
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("UPDATE Student SET Status=@status WHERE StudentID = @selectedStudentID", conn);
            cmd.Parameters.AddWithValue("@selectedStudentID", StudentID);
            cmd.Parameters.AddWithValue("@status", Status);
            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();
            if (count > 0)
                return 0;//success
            else
                return -2;//error
        }
        //MENTOR FUNCTS END-===========


        //SYSADMIN FUNCT
        public int add()
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("Insert into Student (StudentID, Name, EmailAddr, Password, Status, MentorID, Course) Values (@studentid, @name, @emailaddr, 'p@55Student', 'N', @mentorid, @course)", conn);
            SqlCommand cmd1 = new SqlCommand("SET IDENTITY_INSERT Student ON", conn);
            SqlCommand cmd2 = new SqlCommand("SET IDENTITY_INSERT Student OFF", conn);
            cmd.Parameters.AddWithValue("@studentid", StudentID);
            cmd.Parameters.AddWithValue("@name", Name);
            cmd.Parameters.AddWithValue("@emailaddr", Email);
            cmd.Parameters.AddWithValue("@course", Course);
            cmd.Parameters.AddWithValue("@MentorID", MentorID);
            conn.Open();
            cmd1.ExecuteNonQuery();
            cmd.ExecuteNonQuery();
            cmd2.ExecuteNonQuery();
            conn.Close();
            return 0;
        }

        public int getNewID()
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT MAX(StudentID) AS 'ID' FROM Student", conn);
            SqlDataAdapter daStu = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daStu.Fill(result, "StuDetails");
            conn.Close();
            int StuID = Convert.ToInt16(result.Tables["StuDetails"].Rows[0]["ID"]);
            StuID += 1;
            return StuID;
        }
    }
}