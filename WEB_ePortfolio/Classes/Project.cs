﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace WEB_ePortfolio.Classes
{
    public class Project
    {
        private int projectID;
        private string title;
        private string description;
        private string projectPoster;
        private string projectURL;

        public int ProjectID
        {
            get { return projectID; }
            set { projectID = value; }
        }
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public string ProjectPoster
        {
            get { return projectPoster; }
            set { projectPoster = value; }
        }
        public string ProjectURL
        {
            get { return projectURL; }
            set { projectURL = value; }
        }

        public Project() { }

        public int update()
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("UPDATE Project SET Title=@title, Description=@description, ProjectPoster=@projectposter WHERE ProjectID=@projectid", conn);
            cmd.Parameters.AddWithValue("@title", Title);
            cmd.Parameters.AddWithValue("@description", Description);
            cmd.Parameters.AddWithValue("@projectposter", ProjectPoster);
            cmd.Parameters.AddWithValue("@projectid", ProjectID);
            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();

            if (count > 0)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }

        public int add()
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("INSERT INTO Project(ProjectID, Title, Description, ProjectPoster) VALUES (@projectid, @title, @description, @projectposter)", conn);
            cmd.Parameters.AddWithValue("@projectid", ProjectID);
            cmd.Parameters.AddWithValue("@title", Title);
            cmd.Parameters.AddWithValue("@description", Description);
            cmd.Parameters.AddWithValue("@projectposter", ProjectPoster);
            SqlCommand cmd2 = new SqlCommand("SET IDENTITY_INSERT Project ON", conn);
            SqlCommand cmd3 = new SqlCommand("SET IDENTITY_INSERT Project OFF", conn);
            conn.Open();
            cmd2.ExecuteNonQuery();
            cmd.ExecuteNonQuery();
            cmd3.ExecuteNonQuery();
            conn.Close();

            return 0;
        }

        public int getNewID()
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT MAX(ProjectID) AS 'ID' FROM Project", conn);
            SqlDataAdapter daProj = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daProj.Fill(result, "ProjDetails");
            conn.Close();
            int projID = Convert.ToInt16(result.Tables["ProjDetails"].Rows[0]["ID"]);
            projID += 1;

            return projID;
        }

        public int getDetails()
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT * FROM Project WHERE Title=@title", conn);
            cmd.Parameters.AddWithValue("@title", Title);
            SqlDataAdapter daProj = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daProj.Fill(result, "ProjDetails");
            conn.Close();
            if (result.Tables["ProjDetails"].Rows.Count > 0)
            {
                ProjectID = Convert.ToInt16(result.Tables["ProjDetails"].Rows[0]["ProjectID"]);
                Title = result.Tables["ProjDetails"].Rows[0]["Title"].ToString();
                Description = result.Tables["ProjDetails"].Rows[0]["Description"].ToString();
                ProjectPoster = result.Tables["ProjDetails"].Rows[0]["ProjectPoster"].ToString();
                ProjectURL = result.Tables["ProjDetails"].Rows[0]["ProjectURL"].ToString();
                return 0;
            }
            else
            {
                return -1;
            }

            
        }
    }
}