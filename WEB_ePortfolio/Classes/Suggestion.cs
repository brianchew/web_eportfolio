﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_ePortfolio.Classes
{
    public class Suggestion
    {
        private int suggestionID;
        private int mentorID;
        private int studentID;

        public int StudentID
        {
            get { return studentID; }
            set { studentID = value; }
        }


        public int MentorID
        {
            get { return mentorID; }
            set { mentorID = value; }
        }

        public int SuggestionID
        {
            get { return suggestionID; }
            set { suggestionID = value; }
        }
        
    }
}