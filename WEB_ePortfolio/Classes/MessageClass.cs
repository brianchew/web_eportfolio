﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_ePortfolio.Classes
{
    public class MessageClass
    {
        //Attributes
        private int messageID;
        private int fromID;
        private int toID;
        private DateTime dateTimePosted;
        private string title;
        private string text;

        //Properties
        public int MessageID
        {
            get { return messageID; }
            set { messageID = value; }
        }
        public int FromID
        {
            get { return fromID; }
            set { fromID = value; }
        }
        public int ToID
        {
            get { return toID; }
            set { toID = value; }
        }
        public DateTime DateTimePosted
        {
            get { return dateTimePosted; }
            set { dateTimePosted = value; }
        }
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public bool NewMessage(int fromID, int toID, string title, string text)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("INSERT INTO [Message] (FromID, ToID, Title, Text) VALUES (@fromid,@toid,@title,@text)", conn);
            cmd.Parameters.AddWithValue("@fromid", fromID);//parent id
            cmd.Parameters.AddWithValue("@toid", toID);//mentor id
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@text", text);
            conn.Open();
            int cnt = cmd.ExecuteNonQuery();
            conn.Close();
            if (cnt > 1)
            {
                //sent
                return true;
            }
            else
            {
                //not sent
                return false;
            }
        }




    }
}