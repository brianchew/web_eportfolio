﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_ePortfolio.Classes
{
    public class ReplyClass
    {
        //Attributes
        private int replyID;
        private int messageID;//which message id replying to
        private int mentorID;//which mentor is replying
        private int parentID;
        private DateTime dateTimePosted;
        private string text;

        //Props
        public int ReplyID
        {
            get { return replyID; }
            set { replyID = value; }
        }
        public int MessageID
        {
            get { return messageID; }
            set { messageID = value; }
        }
        public int MentorID
        {
            get { return mentorID; }
            set { mentorID = value; }
        }
        public int ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }
        public DateTime DateTimePosted
        {
            get { return dateTimePosted; }
            set { dateTimePosted = value; }
        }
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public bool ReplyMessage()//replies true/false upon successful message
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            //why is the mentorid null and parent id null too?
            SqlCommand cmd = new SqlCommand("INSERT INTO Reply (MessageID, MentorID, DateTimePosted, Text) VALUES (@msgid,@mentorid,@timeposted,@text)", conn);
            //All foreign key constraints must be linked to PK
            cmd.Parameters.AddWithValue("@msgid", MessageID);//parent id is null when made from mentor
            cmd.Parameters.AddWithValue("@mentorid", MentorID);
            //xtras
            cmd.Parameters.AddWithValue("@timeposted", DateTimePosted);
            cmd.Parameters.AddWithValue("@text", Text);
            conn.Open();
            int number = cmd.ExecuteNonQuery();
            conn.Close();
            if (number == 0)
            {
                //not successful
                return false;
            }
            else
            {
                //successful
                return true;
            }
        }
        public bool ReplyMessageParent()
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("INSERT INTO Reply (MessageID, ParentID, DateTimePosted, Text) VALUES (@msgid,@parentid,@timeposted,@text)", conn);
            cmd.Parameters.AddWithValue("@msgid", MessageID);
            cmd.Parameters.AddWithValue("@parentid", ParentID);//mentor id is null when reply made via parent
            cmd.Parameters.AddWithValue("@timeposted", DateTimePosted);
            cmd.Parameters.AddWithValue("@text", Text);
            conn.Open();
            int number = cmd.ExecuteNonQuery();
            conn.Close();
            if (number == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool UpdateReply()
        {
            DateTime newdt = DateTime.Now;
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("UPDATE Reply SET [Text]=@updatedtext,DateTimePosted=@datetimenow WHERE ReplyID=@replyid;", conn);
            cmd.Parameters.AddWithValue("@updatedtext", Text);
            cmd.Parameters.AddWithValue("@datetimenow", newdt);
            cmd.Parameters.AddWithValue("@replyid", ReplyID);
            conn.Open();
            int number = cmd.ExecuteNonQuery();
            conn.Close();
            if (number == 0)
            {
                //not successful
                return false;
            }
            else
            {
                //successful
                return true;
            }
        }


    }
}