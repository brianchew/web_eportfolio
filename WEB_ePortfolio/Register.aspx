﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="WEB_ePortfolio.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style type="text/css">
        .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }

        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
        }

        .form-signin .checkbox {
            font-weight: normal;
        }

        .form-signin .form-control {
            position: relative;
            height: auto;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            padding: 10px;
            font-size: 16px;
        }

        .form-signin .form-control:focus {
            z-index: 2;
        }

        .email-input{
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }

        .password-input{
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="container-fluid h-100 bg-secondary">
        <div class="container shadow-lg pb-5">

            <div class="form-signin">
                <h2 class="form-signin-heading display-5 text-white">Register here!</h2>
                <!--login form-->
                <label for="txtEmail" class="sr-only">Email address</label>
                <asp:TextBox CssClass="form-control email-input" ID="txtEmail" runat="server" TextMode="Email" placeholder="Email address"></asp:TextBox>

                <label for="txtPassword" class="sr-only">Password</label>
                <asp:TextBox CssClass="form-control password-input" ID="txtPassword" runat="server" TextMode="Password" placeholder="Password"></asp:TextBox>

                <label for="txtPassword" class="sr-only">Confirm Password</label>
                <asp:TextBox CssClass="form-control password-input" ID="txtPasswordConfirm" runat="server" TextMode="Password" placeholder="Confirm Password"></asp:TextBox>



                <asp:Button CssClass="btn btn-lg btn-primary btn-block mt-2" ID="btnRegister" runat="server" Text="Register" />
            </div>
        </div>

    </div>
</asp:Content>
