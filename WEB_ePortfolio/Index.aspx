﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WEB_ePortfolio.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="cover-pic h-100">
        <div class="container-fluid">
            <div class="row h-100 justify-content-center">
                <div class="col d-flex justify-content-center align-self-center">
                    <h1 class="text-white text-center display-1">Welcome to<br>
                        ABC Polytechnic.<br />
                    <a href="About.aspx" class="btn-lg btn-primary">Read more about us >></a></h1>

                </div>
                
            </div>
        </div>
    </div>
</asp:Content>
