﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mentor/Mentor.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WEB_ePortfolio.Mentor.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="container-fluid h-100 cover-teacher">
        <div class="container-fluid">
            <div class="row h-100 justify-content-center">
                <div class="col d-flex justify-content-center align-self-center">
                    <h1 class="display-1 text-white text-center">Maintenance in Progress<br>
                        Come back again.</h1>
                </div>
            </div>
        </div>
    </section>



    <div class="h-25">
        <footer id="myFooter">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h2 class="logo"><a href="#">
                            <img class="img-fluid" src="../Assets/logo.png" />
                        </a></h2>
                        <p class="text-white">
                            There are
                        <asp:Label ID="noMentors" runat="server" Text="[noMentors]"></asp:Label>
                            mentors online now.
                        </p>
                        <p class="text-white">
                            <asp:Label ID="lblMentorLoginID" runat="server" Text=""></asp:Label>
                            | MentorID:
                        <asp:Label ID="lblMentorID" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                    <div class="col-sm-2">
                        <h5>Information</h5>
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Courses</a></li>
                            <li><a href="#">Acad Calendar</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2">
                        <h5>About us</h5>
                        <ul>
                            <li><a href="#">Corporate</a></li>
                            <li><a href="#">Contact us</a></li>
                            <li><a href="#">Reviews</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2">
                        <h5>Quick Login</h5>
                        <ul>
                            <li><a href="#">Parents</a></li>
                            <li><a href="#">Students</a></li>
                            <li><a href="#">Lecturers</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <div class="social-networks">
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
                        </div>
                        <button type="button" class="btn btn-default">Contact us</button>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>Copyright &copy; 2018 ABC Polytechnic Education Institution. </p>
            </div>
        </footer>
    </div>

</asp:Content>
