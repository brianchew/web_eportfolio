﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mentor/Mentor.Master" AutoEventWireup="true" CodeBehind="Approve.aspx.cs" Inherits="WEB_ePortfolio.Mentor.Approve" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style3 {
            height: 25px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <br />
        <div class="container-fluid shadow-lg p-5 text-white bg-secondary">
            <div class="bg-success shadow-lg rounded p-3">
                <p>
                    Instructions:
                <br />
                    &emsp; Select on student tuple to load projects the student is a team member of
                <br />
                    &emsp; To approve, click on Finalize Approval with selected student. 
                <br />
                    &emsp; NOTE: Clicking on the finalize approval button will approve the selected student (in blue) for public viewing. 
                    <asp:Label ID="lblMessage" runat="server" Text="" CssClass="text-danger"></asp:Label><asp:Button ID="btnRefresh" runat="server" CssClass="btn btn-primary pl-2" Text="RefreshPage" OnClick="btnRefresh_Click" />
                </p>
            </div>
            <div class="container-fluid mt-3">
                <h4>Students</h4>
                <div class="table-responsive">
                    <asp:GridView ID="gvStudentDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False" AutoGenerateSelectButton="True" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="1" CellSpacing="1" CssClass="table" DataKeyNames="StudentID" ForeColor="Black" HorizontalAlign="Left" OnPageIndexChanging="gvStudentDetails_PageIndexChanging" OnSelectedIndexChanged="gvStudentDetails_SelectedIndexChanged" PageSize="2" EmptyDataText="No students are in need of Approval">
                        <Columns>
                            <asp:BoundField DataField="Course" HeaderText="Course" />
                            <asp:BoundField DataField="StudentID" HeaderText="Student ID" />
                            <asp:BoundField DataField="Name" HeaderText="Name" />
                            <asp:BoundField DataField="EmailAddr" HeaderText="Email address" />
                            <asp:BoundField DataField="ExternalLink" HeaderText="Link" />
                        </Columns>
                        <FooterStyle BackColor="#CCCCCC" />
                        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" />
                        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="Gray" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#383838" />
                    </asp:GridView>
                </div>
                <h4>Student's Projects</h4>
                <asp:SqlDataSource ID="Students" runat="server" ConnectionString="<%$ ConnectionStrings:Student_EPortfolioConnectionString %>" SelectCommand="SELECT [StudentID], [Name], [Course], [ExternalLink], [EmailAddr] FROM [Student]"></asp:SqlDataSource>
                <div class="table-responsive">
                    <asp:GridView ID="gvStudentProjects" runat="server" AutoGenerateColumns="False" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" CssClass="table" ForeColor="Black" PageSize="1">
                        <Columns>
                            <asp:BoundField DataField="ProjectID" HeaderText="ProjectID" />
                            <asp:BoundField DataField="Title" HeaderText="Title" />
                            <asp:BoundField DataField="Description" HeaderText="Description" />
                            <asp:BoundField DataField="Role" HeaderText="Role In Project" />
                            <asp:HyperLinkField Target="_blank" DataNavigateUrlFields="ProjectID" DataNavigateUrlFormatString="ProjDetailsMore.aspx?projectid={0}" NavigateUrl="~/Mentor/ProjDetailsMore.aspx" Text="View more details" />
                        </Columns>
                        <FooterStyle BackColor="#CCCCCC" />
                        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                        <RowStyle BackColor="White" />
                        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#808080" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#383838" />
                    </asp:GridView>
                </div>
                <asp:Button ID="btnConfirmPass" runat="server" CssClass="btn btn-primary" Text="Finalize Approval" OnClick="btnConfirmPass_Click" ToolTip="When you click on this button, it will finalize approval for the select student!" />
            </div>
        </div>
    <div class="h-25">
        <footer id="myFooter">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h2 class="logo"><a href="#">
                            <img class="img-fluid" src="../Assets/logo.png" />
                        </a></h2>
                    </div>
                    <div class="col-sm-2">
                        <h5>Information</h5>
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Courses</a></li>
                            <li><a href="#">Acad Calendar</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2">
                        <h5>About us</h5>
                        <ul>
                            <li><a href="#">Corporate</a></li>
                            <li><a href="#">Contact us</a></li>
                            <li><a href="#">Reviews</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2">
                        <h5>Quick Login</h5>
                        <ul>
                            <li><a href="#">Parents</a></li>
                            <li><a href="#">Students</a></li>
                            <li><a href="#">Lecturers</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <div class="social-networks">
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
                        </div>
                        <button type="button" class="btn btn-default">Contact us</button>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>Copyright &copy; 2018 ABC Polytechnic Education Institution. </p>
            </div>
        </footer>
    </div>
</asp:Content>
