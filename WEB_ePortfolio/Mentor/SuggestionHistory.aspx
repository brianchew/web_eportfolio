﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mentor/Mentor.Master" AutoEventWireup="true" CodeBehind="SuggestionHistory.aspx.cs" Inherits="WEB_ePortfolio.Mentor.SuggestionHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid h-100 bg-secondary">
        <h2 class="text-white">Your suggestion history</h2>
        <div class="table-responsive">
            <asp:GridView ID="gvSuggestionHistory" runat="server" CellPadding="4" CssClass="table" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="SuggestionID" HeaderText="SuggestionID" />
                    <asp:BoundField DataField="StudentID" HeaderText="StudentID" />
                    <asp:BoundField DataField="Name" HeaderText="Name" />
                    <asp:ImageField DataImageUrlField="StudentID" DataImageUrlFormatString="../Assets/StudentImages/{0}.jpg" HeaderText="Photo">
                    </asp:ImageField>
                    <asp:BoundField DataField="Course" HeaderText="Course" />
                    <asp:BoundField DataField="DateCreated" HeaderText="Date Created" />
                    <asp:BoundField DataField="Description" HeaderText="Suggestion" />
                </Columns>
                <EditRowStyle BackColor="#7C6F57" />
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
        </div>
        <div class="container text-center">
            <a href="PostSuggestion.aspx" class="btn btn-primary">Return to PostSuggestion Page</a>
        </div>
    </div>
</asp:Content>
