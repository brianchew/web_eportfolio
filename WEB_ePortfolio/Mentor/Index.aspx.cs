﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_ePortfolio.Mentor
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LoginID"] != null)
            {
                noMentors.Text = Application["NoMentors"].ToString();
                lblMentorLoginID.Text = (string)Session["LoginID"];
                lblMentorID.Text = Session["MentorID"].ToString();
            }
            else
            {
                Response.Redirect("../Login.aspx");
            }


        }
    }
}