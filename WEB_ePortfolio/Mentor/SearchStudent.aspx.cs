﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using WEB_ePortfolio.Classes;
using System.Configuration;

namespace WEB_ePortfolio.Mentor
{
    public partial class SearchStudent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //do smth
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string searchFields = txtboxSearch.Text;
            if (!string.IsNullOrEmpty(searchFields))
            {
                searchForStud(searchFields);
            }
            else
            {
                lblMsg.Text = "Please put in some values. Search result cannot be empty";
            }
        }
        public void searchForStud(string searchFields)
        {
            string[] results = txtboxSearch.Text.TrimEnd().Split(','); //sep by ,
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);

            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE StudentID IN (SELECT StudentID FROM StudentSkillSet t1 INNER JOIN SkillSet t2 ON t1.SkillSetID=t2.SkillSetID WHERE SkillSetName LIKE @searchfield);", conn);//load students who'm already have profiles posted
            cmd.Parameters.AddWithValue("@searchfield", "%"+searchFields+"%");
            SqlDataAdapter daStudentDetails = new SqlDataAdapter(cmd);
            //Create dataset object to contain records
            DataSet result = new DataSet();

            conn.Open();
            daStudentDetails.Fill(result, "searchResults");
            conn.Close();

            gvResults.DataSource = result.Tables["searchResults"];
            gvResults.DataBind();
        }
    }
}