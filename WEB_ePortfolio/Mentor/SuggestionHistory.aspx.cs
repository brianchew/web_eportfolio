﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using WEB_ePortfolio.Classes;

namespace WEB_ePortfolio.Mentor
{
    public partial class SuggestionHistory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int mentorID = Convert.ToInt32(Session["MentorID"]);
                displaySuggestions(mentorID);
            }
        }
        private void displaySuggestions(int id)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT * FROM Suggestion s INNER JOIN Student stud ON s.StudentID=stud.StudentID WHERE s.MentorID=@mentorid", conn);
            cmd.Parameters.AddWithValue("@mentorid", id);
            SqlDataAdapter daMsgs = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daMsgs.Fill(result, "SuggestionHistory");
            conn.Close();

            gvSuggestionHistory.DataSource = result.Tables["SuggestionHistory"];
            gvSuggestionHistory.DataBind();
        }
    }
}