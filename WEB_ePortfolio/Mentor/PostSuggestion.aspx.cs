﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using WEB_ePortfolio.Classes;
using System.Configuration;

namespace WEB_ePortfolio.Mentor
{
    public partial class PostSuggestion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadStudent();
            }
        }
        private void LoadStudent()
        {
            int MentorID = Convert.ToInt16(Session["MentorID"]);
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);

            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Status='Y' AND MentorID=@mentorid", conn);//load students who'm already have profiles posted
            cmd.Parameters.AddWithValue("@mentorid", MentorID);
            SqlDataAdapter daStudentDetails = new SqlDataAdapter(cmd);
            //Create dataset object to contain records
            DataSet result = new DataSet();

            conn.Open();
            daStudentDetails.Fill(result, "StudentDetails");
            conn.Close();

            gvStudentDetails.DataSource = result.Tables["StudentDetails"];
            gvStudentDetails.DataBind();
        }

        protected void gvStudentDetails_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedStudID = Convert.ToInt32(gvStudentDetails.SelectedDataKey[0]);
            StudentClass student = new StudentClass();
            DataSet result = new DataSet();

            student.StudentID = selectedStudID;
            int errorCode = student.loadStudentProjs(ref result);//GET STUDENTPROJ DETAILS from here
            if (errorCode == 0)
            {
                gvStudentProjects.DataSource = result.Tables["StudentProjDetails"];
                gvStudentProjects.DataBind();
            }
        }

        protected void btnPostSuggestion_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                if (gvStudentDetails.SelectedIndex != -1 && !String.IsNullOrEmpty(tbSuggestion.Text))
                {
                    //needed variables
                    int selectedStudID = Convert.ToInt32(gvStudentDetails.SelectedDataKey[0]);
                    int MentorID = Convert.ToInt16(Session["MentorID"]);
                    DateTime datetimenow = DateTime.UtcNow;

                    string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
                    SqlConnection conn = new SqlConnection(strConn);

                    SqlCommand cmd = new SqlCommand("INSERT INTO Suggestion(MentorID,StudentID,Description,Status,DateCreated) VALUES (@mentorid,@studentid,@text,@status,@datecreated);", conn);
                    cmd.Parameters.AddWithValue("@mentorid", MentorID);
                    cmd.Parameters.AddWithValue("@studentid", selectedStudID);
                    cmd.Parameters.AddWithValue("@text", tbSuggestion.Text);
                    cmd.Parameters.AddWithValue("@status", 'N');
                    cmd.Parameters.AddWithValue("@datecreated", datetimenow);
                    conn.Open();
                    int no = cmd.ExecuteNonQuery();
                    conn.Close();
                    if (no > 0)
                    {
                        lblMessage.Text = "Successful action. Please refresh page to update student listing.";
                    }
                    else
                    {
                        lblMessage.Text = "Please try again later";
                    }
                    StudentClass student = new StudentClass();
                    student.StudentID = selectedStudID;
                    student.Status = 'N';
                    student.updateStatus();
                }
                else
                {
                    lblMessage.Text = "Please select a student, & suggestion to be writtien.";
                }

            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            Response.Redirect("PostSuggestion.aspx");
        }
    }
}