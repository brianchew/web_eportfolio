﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mentor/Mentor.Master" AutoEventWireup="true" CodeBehind="MentorChat.aspx.cs" Inherits="WEB_ePortfolio.Mentor.MentorChat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <nav class="navbar navbar-expand-lg navbar-dark bg-danger">
        <div class="container">
            <a class="navbar-brand" href="#">Mentor Menu </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <asp:Button ID="btnRefresh" runat="server" CssClass="btn btn-warning" Text="Refresh Page" OnClick="btnRefresh_Click" />
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid h-100 bg-secondary">
        <br />
        <div class="container-fluid shadow-lg">
            <section class="h-75 rounded my-5 table-responsive">
                <br />
                <br />
                <asp:GridView ID="gvMessages" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" CssClass="table" ForeColor="Black" GridLines="Vertical" AutoGenerateSelectButton="True" DataKeyNames="MessageID" OnSelectedIndexChanged="gvMessages_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <Columns>
                        <asp:BoundField DataField="MessageID" HeaderText="Message ID" />
                        <asp:BoundField DataField="Title" HeaderText="Title" />
                        <asp:BoundField DataField="DateTimePosted" HeaderText="Date Posted" />
                        <asp:BoundField DataField="ParentName" HeaderText="Parent Name" />
                        <asp:BoundField DataField="EmailAddr" DataFormatString="&lt;a href=mailto:{0}&gt;{0}&lt;/a&gt;" HeaderText="Parent's Email" HtmlEncodeFormatString="False" />
                        <asp:BoundField DataField="Text" HeaderText="Message" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="Gray" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                </asp:GridView>
            </section>
            <section class="h-25 table-responsive">
                <h3 class="text-white">Parent Replies</h3>
                <asp:GridView ID="gvRepliesFromParent" runat="server" CssClass="table" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical" DataKeyNames="The parent has not yet replied to your message">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <Columns>
                        <asp:BoundField DataField="ReplyID" HeaderText="ReplyID" />
                        <asp:BoundField DataField="DateTimePosted" HeaderText="DateTime Posted" />
                        <asp:BoundField DataField="Text" HeaderText="Reply" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                </asp:GridView>
                <h3 class="text-white">Mentor Replies:</h3>
                <asp:GridView ID="gvReplies" runat="server" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="3" CellSpacing="2" CssClass="table" ForeColor="Black" AutoGenerateColumns="False" EmptyDataText="You have yet to reply this parent" OnRowCommand="gvReplies_RowCommand">
                    <Columns>
                        <asp:BoundField DataField="ReplyID" HeaderText="ReplyID" />
                        <asp:BoundField DataField="DateTimePosted" HeaderText="DateTime Posted" />
                        <asp:TemplateField HeaderText="Reply">
                            <ItemTemplate>
                                <asp:TextBox ID="tbUpdateReply" runat="server" Text='<%# Eval("Text") %>' CssClass="w-100"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField ButtonType="Button" CommandName="UpdateReply" Text="Update" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                    <RowStyle BackColor="White" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                </asp:GridView>
                <asp:Label ID="lblReplyStatus" runat="server" Text=""></asp:Label>
                <div class="row">
                    <div class="col-md-9">
                        <asp:TextBox ID="txtReply" runat="server" CssClass="form-control"></asp:TextBox>

                    </div>
                    <div class="col-md-3">
                        <asp:Button ID="btnReply" runat="server" Text="Reply here" CssClass="form-control btn btn-primary" OnClick="btnReply_Click" />
                    </div>
                </div>
            </section>
        </div>
    </div>
</asp:Content>
