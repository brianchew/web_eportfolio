﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MentorMenu.ascx.cs" Inherits="WEB_ePortfolio.Mentor.MentorMenu1" %>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
    <div class="container">
        <a class="navbar-brand" href="../Index.aspx" style="font-size: 32px; font-weight: bold; color: white;">ABC Polytechnic
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#staffNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="staffNavbar">
            <ul class="navbar-nav mr-auto ">
                <li class="nav-item">
                    <a class="nav-link" href="Index.aspx">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="Approve.aspx">Approve</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="PostSuggestion.aspx">Suggestions</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="SearchStudent.aspx">Search</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="MentorChat.aspx">Chat</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="MentorPassword.aspx">Manage Password</a>
                </li>

            </ul>
            <ul class="navbar-nav ml-auto">
                <asp:Button ID="btnLogOut" class="btn btn-primary" runat="server" Text="Log Out" OnClick="btnLogOut_Click" />
            </ul>
        </div>
    </div>
</nav>
