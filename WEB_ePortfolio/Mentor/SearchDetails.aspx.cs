﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using WEB_ePortfolio.Classes;
using System.Configuration;

namespace WEB_ePortfolio.Mentor
{
    public partial class SearchDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

            }
            if (Request.QueryString["studentid"] != null)
            {
                int studid = Convert.ToInt16(Request.QueryString["studentid"]);
                string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();

                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE StudentID = @studid", conn);
                DataSet result = new DataSet();

                cmd.Parameters.AddWithValue("@studid", studid);
                SqlDataAdapter daStudentProjs = new SqlDataAdapter(cmd);

                conn.Open();
                daStudentProjs.Fill(result, "StudentDetails");
                conn.Close();

                gvDetails.DataSource = result.Tables["StudentDetails"];
                gvDetails.DataBind();
            }
        }
    }
}