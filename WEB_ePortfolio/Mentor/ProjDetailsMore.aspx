﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mentor/Mentor.Master" AutoEventWireup="true" CodeBehind="ProjDetailsMore.aspx.cs" Inherits="WEB_ePortfolio.Mentor.ProjDetailsMore" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="container-fluid h-100 bg-secondary">
        <div class="table-responsive">
            <asp:GridView ID="GridView1" runat="server" CellPadding="4" CssClass="table" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:BoundField DataField="Title" HeaderText="Project Name" />
                    <asp:ImageField DataImageUrlField="ProjectPoster" DataImageUrlFormatString="../Assets/ProjectImages/{0}" HeaderText="Project Poster"  ControlStyle-CssClass="img-fluid">
                    </asp:ImageField>
                    <asp:BoundField DataField="Description" HeaderText="Description" />
                    <asp:BoundField DataField="ProjectURL" HeaderText="ProjectURL" />
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />

            </asp:GridView>
        </div>
    </section>
</asp:Content>
