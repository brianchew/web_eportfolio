﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WEB_ePortfolio.Mentor
{
    public partial class ProjDetailsMore : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["projectid"] != null)
            {
                int selectedProjID = Convert.ToInt32(Request.QueryString["projectid"]);
                string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();

                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("SELECT * FROM Project WHERE ProjectID = @projectid", conn);
                DataSet result = new DataSet();

                cmd.Parameters.AddWithValue("@projectid", selectedProjID);
                SqlDataAdapter daStudentProjs = new SqlDataAdapter(cmd);

                conn.Open();
                daStudentProjs.Fill(result, "StudentProjDetails");
                conn.Close();

                GridView1.DataSource = result.Tables["StudentProjDetails"];
                GridView1.DataBind();
            }
        }
    }
}