﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mentor/Mentor.Master" AutoEventWireup="true" CodeBehind="SearchDetails.aspx.cs" Inherits="WEB_ePortfolio.Mentor.SearchDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="container-fluid h-100 bg-secondary">
        <div class="table-responsive">
        <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" CellPadding="4" CssClass="table" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="StudentID" HeaderText="StudentID" />
                <asp:BoundField DataField="Name" HeaderText="Name" />
                <asp:BoundField DataField="Course" HeaderText="Course" />
                <asp:BoundField DataField="Description" HeaderText="Description" />
                <asp:BoundField DataField="EmailAddr" HeaderText="Email Address" />
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
            </div>
    </section>
</asp:Content>
