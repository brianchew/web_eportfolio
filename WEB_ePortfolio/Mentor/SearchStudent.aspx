﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mentor/Mentor.Master" AutoEventWireup="true" CodeBehind="SearchStudent.aspx.cs" Inherits="WEB_ePortfolio.Mentor.SearchStudent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid h-auto bg-secondary">
        <br />
        <div class="container shadow-lg h-75">
            <div class="row">
                <div class="col-sm-10">
                    <asp:TextBox ID="txtboxSearch" runat="server" CssClass="form-control" placeholder="Search by skillset"></asp:TextBox>

                </div>
                <div class="col-sm-2 shadow-none">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn-lg btn-success btn-block" OnClick="btnSearch_Click" />

                </div>
            </div>

        </div>
        <div class="container-fluid h-100">
                        <div class="row">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <asp:GridView ID="gvResults" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" CssClass="table" EmptyDataText="Your result was not found.">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                <asp:BoundField DataField="Course" HeaderText="Course" />
                                <asp:ImageField DataImageUrlField="StudentID" DataImageUrlFormatString="../Assets/StudentImages/{0}.jpg" HeaderText="Photo">
                                </asp:ImageField>
                                <asp:HyperLinkField Text="More Details" DataNavigateUrlFields="StudentID" DataNavigateUrlFormatString="SearchDetails.aspx?studentid={0}" />
                            </Columns>
                            <EditRowStyle BackColor="#7C6F57" />
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#E3EAEB" />
                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F8FAFA" />
                            <SortedAscendingHeaderStyle BackColor="#246B61" />
                            <SortedDescendingCellStyle BackColor="#D4DFE1" />
                            <SortedDescendingHeaderStyle BackColor="#15524A" />
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
