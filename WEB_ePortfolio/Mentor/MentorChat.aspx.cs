﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using WEB_ePortfolio.Classes;
using System.Diagnostics;


namespace WEB_ePortfolio.Mentor
{
    public partial class MentorChat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int mentorID = Convert.ToInt32(Session["MentorID"]);
                displayMessages(mentorID);
            }

        }
        private void displayMessages(int id)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT * FROM Message m INNER JOIN Parent p ON m.FromID=p.ParentID WHERE ToID=@mentorid", conn);
            cmd.Parameters.AddWithValue("@mentorid", id);
            SqlDataAdapter daMsgs = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daMsgs.Fill(result, "Messages");
            conn.Close();

            gvMessages.DataSource = result.Tables["Messages"];
            gvMessages.DataBind();
        }
        /*
        private void retrieveMsgTitles(int id)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            DataSet result = new DataSet();

            SqlCommand cmd = new SqlCommand("SELECT * FROM [Message] WHERE ToID=@mentorid;", conn);
            cmd.Parameters.AddWithValue("@mentorid", id);

            SqlDataAdapter daTitles = new SqlDataAdapter(cmd);

            conn.Open();
            daTitles.Fill(result, "Titles");
            conn.Close();
            if (result.Tables["Titles"].Rows.Count > 0)
            {
                ddlWhichMessage.DataSource = result.Tables["Titles"];
                ddlWhichMessage.DataTextField = "Title";
                ddlWhichMessage.DataValueField = "MessageID";
                ddlWhichMessage.DataBind();
                ddlWhichMessage.Items.Insert(0, "-Select-");

            }
            else
            {
                lblReplyStatus.Text = "Please try again later";
            }
        }
        */
        private int getParentID(int messageID)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT FromID FROM Message WHERE MessageID=@messageid;", conn);
            cmd.Parameters.AddWithValue("@messageid", messageID);
            conn.Open();
            int id = Convert.ToInt32(cmd.ExecuteScalar());
            conn.Close();
            return id;
        }
        private void displayReplies(int id,int msgid)//your own replies
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT * FROM Reply WHERE MentorID=@mentorid AND MessageID=@msgid AND ParentID IS NULL", conn);
            cmd.Parameters.AddWithValue("@mentorid", id);
            cmd.Parameters.AddWithValue("@msgid", msgid);
            SqlDataAdapter daReplies = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daReplies.Fill(result, "Replies");
            conn.Close();

            gvReplies.DataSource = result.Tables["Replies"];
            gvReplies.DataBind();
        }
        private void displayRepliesFromParent(int msgid)//replies from parent
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            int parentid = getParentID(msgid);
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT * FROM Reply WHERE ParentID=@parentid AND MessageID=@msgid AND MentorID IS NULL", conn);
            cmd.Parameters.AddWithValue("@parentid", parentid);
            cmd.Parameters.AddWithValue("@msgid", msgid);
            SqlDataAdapter daReplies = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daReplies.Fill(result, "RepliesParent");
            conn.Close();

            gvRepliesFromParent.DataSource = result.Tables["RepliesParent"];
            gvRepliesFromParent.DataBind();
        }
        protected void btnReply_Click(object sender, EventArgs e)
        {
            if (gvMessages.SelectedIndex != -1)
            {
                if (!(string.IsNullOrEmpty(txtReply.Text)))
                {
                    int msgID = Convert.ToInt32(gvMessages.SelectedDataKey[0]);
                    int mentorID = Convert.ToInt32(Session["MentorID"]);
                    ReplyClass objReply = new ReplyClass();
                    objReply.MessageID = msgID;
                    objReply.MentorID = mentorID;
                    objReply.ParentID = getParentID(objReply.MessageID);
                    objReply.DateTimePosted = DateTime.UtcNow;
                    objReply.Text = txtReply.Text.ToString();

                    if (objReply.ReplyMessage())
                    {
                        lblReplyStatus.Text = "Reply was sent to the database.";
                    }
                    else
                    {
                        lblReplyStatus.Text = "Please try again later.";

                    }
                    //Debug
                    Debug.WriteLine("MessageID used:" + objReply.MessageID);
                    Debug.WriteLine("MentorID used:" + objReply.MentorID);
                    Debug.WriteLine("ParentID used:" + objReply.ParentID);
                    Debug.WriteLine("Datetime used:" + objReply.DateTimePosted);
                    Debug.WriteLine("Message sent:" + objReply.Text);
                    //-
                }
                else
                {
                    lblReplyStatus.Text = "Are you sure you want to send nothingness?!";
                }


            }
            else
            {
                lblReplyStatus.Text = "Please select a message to reply to!!";

            }

        }

        protected void gvMessages_SelectedIndexChanged(object sender, EventArgs e)
        {
            //load replies
            //get selected
            int msgID = Convert.ToInt32(gvMessages.SelectedDataKey[0]);
            int mentorID = Convert.ToInt32(Session["MentorID"]);
            displayRepliesFromParent(msgID);
            displayReplies(mentorID, msgID);
        }
        protected void gvReplies_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Debug.WriteLine("gvReplies row command executed");
            if (e.CommandName == "UpdateReply")
            {
                Debug.WriteLine("went to if, command name found");
                int msgID = Convert.ToInt32(gvMessages.SelectedDataKey[0]);
                int i = Convert.ToInt32(e.CommandArgument);
                TextBox tb = (TextBox)gvReplies.Rows[i].FindControl("tbUpdateReply");
                string newReply = tb.Text;
                int replyID = Convert.ToInt32(gvReplies.Rows[i].Cells[0].Text); 
                ReplyClass objReply = new ReplyClass();
                objReply.Text = newReply;
                objReply.ReplyID = replyID;
                if (objReply.UpdateReply())
                {
                    lblReplyStatus.Text = "Your reply for MsgID:" + msgID + " has been updated.";
                }
                else
                {
                    lblReplyStatus.Text = "The reply update did not work, try again!";
                }
                Debug.WriteLine("ReplyID:" + replyID);
                Debug.WriteLine("Updated text:" + newReply);
              
            }
            else
            {
                Debug.WriteLine("went to else, command name not found");
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            Response.Redirect("MentorChat.aspx");
        }
    }

}