﻿<%@ Page Async="true" Title="" Language="C#" MasterPageFile="~/Mentor/Mentor.Master" AutoEventWireup="true" CodeBehind="MentorPassword.aspx.cs" Inherits="WEB_ePortfolio.Mentor.MentorPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .bg-secondary2 {
            background-color: #3c3d41;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container shadow-lg mb-5 p-5 card text-white bg-secondary2">
        <h2>Password requirements</h2>
        <ol>
            <li>At least 8 characters</li>
            <li>At least 1 digit (numbers)</li>
            <li>At least an Uppercase letter</li>
        </ol>
        <asp:Label ID="lblStatus" runat="server" Text="" CssClass="text-danger"></asp:Label>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Old Password</label>
                <asp:TextBox ID="tbCurrentPass" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group col-md-4">
                <label for="inputPassword4">Confirm Old Password</label>
                <asp:TextBox ID="tbCurrentPassConfirm" runat="server" CssClass="form-control" placeholder="Enter Old Pass"></asp:TextBox>
            </div>
            <div class="form-group col-md-2">
                <label for="inputPassword4">Show password</label>
                <asp:Button ID="btnShowPass" runat="server" CssClass="btn btn-primary" Text="Show password" OnClick="btnShowPass_Click" />
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="conpass">New password</label>
                <asp:TextBox ID="tbNewPass" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group col-md-6">
                <label for="conpass2">Confirm new Password</label>
                <asp:TextBox ID="tbConfirmNew" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
            </div>
        </div>
        <div class="form-row">
            <div class="col-sm-12">
            </div>
        </div>
        <asp:Button ID="btnConfirmPass" runat="server" CssClass="btn btn-primary" Text="Confirm Change" OnClick="btnConfirmPass_Click"/>
    </div>
</asp:Content>
