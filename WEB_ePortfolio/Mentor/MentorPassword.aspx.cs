﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using WEB_ePortfolio.Classes;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace WEB_ePortfolio.Mentor
{
    public partial class MentorPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbCurrentPass.Attributes["type"] = "password";
            tbCurrentPassConfirm.Attributes["type"] = "password";

            if (!Page.IsPostBack)
            {
                InitData();
            }
        }
        private void InitData()
        {
            int mentorID = Convert.ToInt32(Session["MentorID"]);
            tbCurrentPass.Text = "";
            tbNewPass.MaxLength = '8';
            tbConfirmNew.MaxLength = '8';
        }

        
        private string LoadPassword(int id)
        {
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);

            SqlCommand cmd = new SqlCommand("SELECT Password FROM Mentor WHERE MentorID=@mentorid", conn);
            cmd.Parameters.AddWithValue("@mentorid", id);
            SqlDataAdapter daMentorPass = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            daMentorPass.Fill(result, "MentorPass");
            conn.Close();
            string password = result.Tables["MentorPass"].Rows[0]["Password"].ToString();
            return password;
        }
        





        private bool ValPass(string oldpass,string oldpass_confirm, string newpass, string newpass_confirm)//tbcurrentpass, tbnewpass, tbconfirmnew
        {
            int mentorID = Convert.ToInt32(Session["MentorID"]);
            int l1 = newpass.Length;
            if (newpass == newpass_confirm && LoadPassword(mentorID) == oldpass && oldpass==oldpass_confirm&& l1>=8 && newpass.Any(char.IsDigit) && newpass.Any(char.IsUpper))
            {
                return true;
            }
            else
                return false;
        }





        private void UpdatePass(string newpass)
        {
            int MentorID = Convert.ToInt16(Session["MentorID"]);
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);

            SqlCommand cmd = new SqlCommand("UPDATE Mentor SET Password=@newpass WHERE MentorID=@mentorid;", conn);
            cmd.Parameters.AddWithValue("@mentorid", MentorID);
            cmd.Parameters.AddWithValue("@newpass", newpass);
            conn.Open();
            int no = cmd.ExecuteNonQuery();
            conn.Close();
            bool suc = false;
            if (no > 0)
            {
                lblStatus.Text = "Successful action. Your password has been changed.";
                suc = true;
                //UpdatePassAsync();
            }
            else
            {
                lblStatus.Text = "Please try again later";
            }
            if (suc)
            {
                //await redirectPage();
            }
        }


        private async Task redirectPage()
        {
            Session.Abandon();
            Server.TransferRequest("Index.aspx", false);
            await Task.Delay(5000);
        }
        /*
        private void UpdatePassAsync()
        {
            Task.Delay(5000);
            Session.Abandon();
            Response.Redirect("../Index.aspx");
        }
        */
        protected void btnShowPass_Click(object sender, EventArgs e)
        {
            tbCurrentPass.Attributes["type"] = "";
            tbCurrentPassConfirm.Attributes["type"] = "";

        }

        protected void btnConfirmPass_Click(object sender, EventArgs e)
        {
            if (ValPass(tbCurrentPassConfirm.Text,tbCurrentPass.Text,tbNewPass.Text,tbConfirmNew.Text))
                UpdatePass(tbConfirmNew.Text);
            else
                lblStatus.Text = "Please key in all fields & Your new password fields may not be the same, or does not meet the minimum requirements.";
        }
    }
}