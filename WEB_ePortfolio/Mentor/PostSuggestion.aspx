﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mentor/Mentor.Master" AutoEventWireup="true" CodeBehind="PostSuggestion.aspx.cs" Inherits="WEB_ePortfolio.Mentor.PostSuggestion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <nav class="navbar navbar-expand-lg navbar-dark bg-danger">
        <div class="container">
            <a class="navbar-brand" href="#">Mentor Menu </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <asp:Button ID="btnRefresh" runat="server" CssClass="btn btn-warning" Text="Refresh Page" OnClick="btnRefresh_Click" />
                    </li>&nbsp;
                    <li class="nav-item">
                        <a href="SuggestionHistory.aspx" class="btn btn-primary">Suggestion History</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid card text-white shadow-lg bg-secondary form-group mb-0">
        <asp:Label ID="lblMessage" runat="server" Text="" CssClass="text-white"></asp:Label>
        <h4>Students</h4>
        <div class="table-responsive">
            <asp:GridView ID="gvStudentDetails" runat="server" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" CssClass="table" ForeColor="Black" AutoGenerateSelectButton="True" DataKeyNames="StudentID" OnSelectedIndexChanged="gvStudentDetails_SelectedIndexChanged" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="StudentID" HeaderText="StudentID" />
                    <asp:BoundField DataField="Name" HeaderText="Name" />
                    <asp:ImageField DataImageUrlField="StudentID" DataImageUrlFormatString="../Assets/StudentImages/{0}.jpg" HeaderText="Photo">
                    </asp:ImageField>
                    <asp:BoundField DataField="Course" HeaderText="Course" />
                    <asp:BoundField DataField="Description" HeaderText="Description" />
                    <asp:BoundField DataField="Achievement" HeaderText="Achievement" />
                    <asp:BoundField DataField="ExternalLink" HeaderText="External Link" />
                    <asp:BoundField DataField="EmailAddr" HeaderText="Email Address" />
                </Columns>
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                <RowStyle BackColor="White" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
            </asp:GridView>
        </div>
        <h4>Student Projects</h4>
        <div class="table-responsive">
            <asp:GridView ID="gvStudentProjects" runat="server" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" CssClass="table" ForeColor="Black" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="Title" HeaderText="Project Name" />
                    <asp:ImageField DataImageUrlField="ProjectPoster" DataImageUrlFormatString="../Assets/ProjectImages/{0}" HeaderText="Project Poster" ControlStyle-CssClass="img-fluid">
                    </asp:ImageField>
                    <asp:BoundField DataField="Description" HeaderText="Description" />
                    <asp:BoundField DataField="ProjectURL" HeaderText="Project URL" />
                </Columns>
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                <RowStyle BackColor="White" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
            </asp:GridView>
        </div>
        <h4>Post Suggestion</h4>
        <div class="form-group">
            <asp:TextBox ID="tbSuggestion" TextMode="multiline" Columns="50" Rows="5" runat="server" CssClass="form-control" />
            <asp:Button ID="btnPostSuggestion" runat="server" CssClass="btn btn-primary ml-3" Text="Post Suggestion" OnClick="btnPostSuggestion_Click" />
        </div>
    </div>
        <div class="h-25">
        <footer id="myFooter">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h2 class="logo"><a href="#">
                            <img class="img-fluid" src="../Assets/logo.png" />
                        </a></h2>
                    </div>
                    <div class="col-sm-2">
                        <h5>Information</h5>
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Courses</a></li>
                            <li><a href="#">Acad Calendar</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2">
                        <h5>About us</h5>
                        <ul>
                            <li><a href="#">Corporate</a></li>
                            <li><a href="#">Contact us</a></li>
                            <li><a href="#">Reviews</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2">
                        <h5>Quick Login</h5>
                        <ul>
                            <li><a href="#">Parents</a></li>
                            <li><a href="#">Students</a></li>
                            <li><a href="#">Lecturers</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <div class="social-networks">
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
                        </div>
                        <button type="button" class="btn btn-default">Contact us</button>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <p>Copyright &copy; 2018 ABC Polytechnic Education Institution. </p>
            </div>
        </footer>
    </div>
</asp:Content>
