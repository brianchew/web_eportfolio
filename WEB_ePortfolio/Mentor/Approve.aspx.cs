﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using WEB_ePortfolio.Classes;
using System.Configuration;

namespace WEB_ePortfolio.Mentor
{
    public partial class Approve : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnRefresh.Visible = false;
            if (!Page.IsPostBack)
            {
                LoadStudent();
            }
        }
        private void LoadStudent()
        {
            int MentorID = Convert.ToInt16(Session["MentorID"]);
            string strConn = ConfigurationManager.ConnectionStrings["WEBePortConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);

            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Status='N' AND MentorID=@mentorid", conn);
            cmd.Parameters.AddWithValue("@mentorid", MentorID);
            SqlDataAdapter daStudentDetails = new SqlDataAdapter(cmd);
            //Create dataset object to contain records
            DataSet result = new DataSet();

            conn.Open();
            daStudentDetails.Fill(result, "StudentDetails");
            conn.Close();

            gvStudentDetails.DataSource = result.Tables["StudentDetails"];
            gvStudentDetails.DataBind();
        }

        protected void gvStudentDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvStudentDetails.PageIndex = e.NewPageIndex;
            LoadStudent();
        }

        protected void gvStudentDetails_SelectedIndexChanged(object sender, EventArgs e)//load student projects
        {
            int selectedStudID = Convert.ToInt32(gvStudentDetails.SelectedDataKey[0]);
            StudentClass student = new StudentClass();
            DataSet result = new DataSet();

            student.StudentID = selectedStudID;
            int errorCode = student.loadStudentProjs(ref result);
            if (errorCode == 0)
            {
                gvStudentProjects.DataSource = result.Tables["StudentProjDetails"];
                gvStudentProjects.DataBind();
            }
        }

        protected void btnConfirmPass_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (gvStudentDetails.SelectedIndex!=-1)
                {
                    //List<int> numbers = new List<int> { 5, 4, 3, 2, 1 };
                    int selectedStudID = Convert.ToInt32(gvStudentDetails.SelectedDataKey[0]);
                    StudentClass student = new StudentClass();
                    student.StudentID = selectedStudID;
                    student.Status = 'Y';//update with Y.
                    int no = student.updateStatus();
                    if (no == 0)
                    {
                        lblMessage.Text = "Successfully approved student. Please proceed the refresh the page. <br>";
                        btnRefresh.Visible = true;
                        /*
                        foreach(int num in numbers)
                        {
                            string strnum = num.ToString();
                            lblMessage.Text = "Page will refresh in... ";
                            lblMessage.Text += strnum + " seconds";
                            Response.AddHeader("Refresh", "5");
                        }
                        CountDownAsync();*/
                    }
                    else if (no == -2)
                    {
                        lblMessage.Text = "Unable to approve student. Please check again.";
                    }
                }
                else
                {
                    lblMessage.Text = "Please select a student to be approved.";
                }

            }

        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            Response.Redirect("Approve.aspx");
        }
        /*
private static async Task<string> CountDownAsync()
{
   List<string> numbers = new List<string> { "1", "2", "3", "4", "5" };
   string x="";
   foreach (string no in numbers)
   {
       x += "Refreshing the page in..." + no + " seconds";
       await Task.Delay(TimeSpan.FromSeconds(1));
       return x;
   }
}
*/
    }
}