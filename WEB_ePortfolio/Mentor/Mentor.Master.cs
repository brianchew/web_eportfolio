﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_ePortfolio.Mentor
{
    public partial class Mentor : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LoginID"] != null)//validates user to be logged in
            {
                //logged in
            }
            else
            {
                //prompt user to login
                Response.Redirect("../Login.aspx");
            }
        }
    }
}